@section('title')
{{"The Trademark Company"}}
@stop


@section('description')
{{ ""}}
@stop

@extends('index')

@section('content')

<div class="content-section-a" style="margin-top:-30px">

    <div class="container">
        <div class="row">
            <div class="line col-md-12 col-sm-12" itemscope itemtype="http://schema.org/Question">


<h3 class=""><strong>Changes and renewals of your registered trademarks</strong></h3><br>
<div class="tb f28"><strong><span class="tx f30" itemprop="name">Can my registered trademark be amended?</span></strong></div>
<br>
<div class="tb f28" itemprop="text">
<span class="tx">You are able to amend your trademark at any point, however these changes are </span><span class="tx">minimal and are limited to only your name and address. For example, you could </span><span class="tx">update your new surname after marriage, but you would not be able to update an </span><span class="tx">entirely new name.</span></div>
<br>
<div class="tb f28"><strong><span class="tx f30" itemprop="name">Can I cancel my registered trademark?</span></strong></div>
<br>
<div class="tb f28">
<span class="tx" itemprop="text">At any time you are able to surrender yours rights to your registered trademark.</span></div>
<br>
<div class="tb f28">
<strong><span class="tx f30" itemprop="name">How do I sell my registered trademark?</span></strong></div>
<br>
<div class="tb f28" itemprop="text">
<span class="tx">By registering a trademark you automatically own it, and it can then be sold just like </span><span class="tx">any other possession. However, if a trademark is sold the new owner needs to be </span><span class="tx">registered, and the trademark needs to be assigned to them.  </span></div>
<br>
<div class="tb f28"></div>
<div class="tb f28"><strong><span class="tx f30" itemprop="name">How long does a trade mark last?</span></strong></div>
<br>
<div class="tb f28">
<span class="tx" itemprop="text">A registered trademark needs to be renewed every 10 years, in order to keep your </span><span class="tx">brand protected. If you instruct us to, we can take care of this on your behalf.</span></div>
<br>
<div class="tb f28">
<strong><span class="tx f30" itemprop"name">What happens if I miss the deadline for my trademark renewal?</span></strong></div>
<br>
<div class="tb f28">
<span class="tx" itemprop="text">If you realise you have forgotten to renew you trademark within six months of the </span><span class="tx">renewal date, you are able to apply to have your trademark restored.</span></div>


<br><br>

<a href="{{ asset('pdf/Changes and Renewals of your Registered Trade Marks.pdf') }}" class="cp-green">Download PDF</a>

           </div>
        </div>
    </div>
</div>
@endsection