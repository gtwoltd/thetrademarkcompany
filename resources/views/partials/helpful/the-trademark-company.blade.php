@section('title')
{{"The Trademark Company"}}
@stop


@section('description')
{{ ""}}
@stop

@extends('index')

@section('content')

<div class="content-section-a" style="margin-top:-30px" itemprop="itemListElement" itemscope
      itemtype="http://schema.org/ListItem">

    <div class="container">
        <div class="row">
            <div class="line col-md-12 col-sm-12">

<h3><strong><span class="tx">The Trademark Company’s service and how we work</span></strong></h3>
<br>
<div class="tb f104"></div>
<div class="tb f104" itemprop="item"><strong><span class="tx f108">Step 1</span></strong><br>
<span class="tx">To begin your application simply type in your chosen trademark via our online </span><span class="tx">trademark <a href="{{ url('') }}#search"> search tool</a></span><span class="tx"> and follow the simple </span><span class="tx">instructions.</span></div>
<br>
<div class="tb f104"></div>
<div class="tb f104" itemprop="item"><strong><span class="tx f108">Step 2</span></strong><br>
<span class="tx">Once you have entered your trademark you will be instructed to select what is </span><span class="tx">known as the </span><span class="tx f108">“Trademark Classification” </span><span class="tx">.  This is a system that is divided </span><span class="tx">between Goods and Services and you must select those that are best suited to </span><span class="tx">your business.</span>
<span class="tx f112">For Example:  If your business was selling cars, you would select ‘Class 12.  Vehicles </span><span class="tx f112">and Locomotives Products’.</span></div>

<div class="tb f104"><span class="tx">If you are not 100% sure, then don’t be worried as our Trademark Attorneys </span><span class="tx">will ensure your trademark is fully protected once you submit your order.</span></div>
<br>
<div class="tb f104"></div>
<div class="tb f104" itemprop="item"><strong><span class="tx f108">Step 3</span></strong><br>
<span class="tx">Once you have submitted your Classifications, our software will check the </span><span class="tx">databases for both the UK and EU registers.  This process will ensure that there </span><span class="tx">are no obvious issues or problems with your chosen trademark.  There may well </span><span class="tx">be a company already with a name similar to yours who also have selected the </span><span class="tx">same classifications.</span></div>
<br><br>


<div class="tb f104" itemprop="item"><strong><span class="tx f108">Step 4</span></strong><br>
<span class="tx">When the search is completed, we can inform you whether your trademark is </span><span class="tx">available and will give you a quote for the trademark application fee.  To proceed </span><span class="tx">with registering your application, you will be asked to fill in your credit or debit </span><span class="tx">card details.</span></div>
<br>
<span class="tx">Please note that we will not take payment at this stage. We have further checks and need
to be 100% satisfied that your trademark has passed all of our checks before submitting your
application.</span>
<br><br>

<div itemprop="item">
<span class="tx"><strong>Step 5</strong></span>
<br><br>

<span class="tx">Only once we are satisfied that your application passes our stringent searches to ensure that:
<br>
a) it complies with the Trade Marks Act<br>
b) that you will not face objections from other trademark owners</span>
<br><br>

<span class="tx">We will inform you that we will be processing your trademark and submit your application for
registration.</span>
<br><br>

<span class="tx">Should there be any reason why your trademark does not pass our checking system, we will
inform you and report our findings as to why this is the case. We will also advise you how
you could amend your trademark in order to be successful with your application.</span>
<br><br>
</div>

<div itemprop="item">
<span class="tx"><strong>Step 6</strong></span>
<br><br>

<span class="tx">For UK Trade Marks:</span>
<br><br>

<span class="tx">Once we have filed your application, you will receive a confirmation email that it has been
processed and payment has been received. You will also receive a copy of your application
form. Your application will then be examined to determine whether the trademark is
acceptable. This process usually takes around four months. A registered trademark lasts 10
years.</span>
<br><br>

<span class="tx">You are able to follow the process of your trademark application by logging onto:
<a href="https://www.gov.uk/track-a-trade-mark">https://www.gov.uk/track-a-trade-mark</a></span>
<br><br>

<span class="tx">For EU Trade Marks:</span>
<br><br>

<span class="tx">The process is slightly different for registering your trademark in the EU. We will email your
filing receipt to you once we have submitted it. It will then be examined and a search report
will be issued showing any similarities or questions from the examiner.</span>
<br><br>

<span class="tx">It will then be published in the Trade Marks Journal to enable any oppositions from trademark
owners; of which they have three months to oppose.</span>
<br>
<span class="tx">To follow the process of your EU application go to: <a href="http://oami.europa.eu/eSearch/">http://oami.europa.eu/eSearch/</a></span>

<br><br>

<a href="{{ asset('pdf/How We Work.pdf') }}" class="cp-green">Download PDF</a>
</div>

           </div>
        </div>
    </div>
</div>
@endsection