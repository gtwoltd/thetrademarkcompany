@section('title')
{{"The Trademark Company"}}
@stop


@section('description')
{{ ""}}
@stop

@extends('index')

@section('content')

<div class="content-section-a" style="margin-top:-30px">

    <div class="container">
        <div class="row">
            <div class="line col-md-12 col-sm-12">

<h3><strong><span class="tx f132">Classification of goods (class 1 - 45)</span></strong></h3>
<br>
<strong><span class="tx f132">Class 1.  Chemical Products</span></strong>
<br>
<span class="tx">Chemicals used in industry, science and photography, as well as in agriculture,</span><span class="tx"> horticulture and forestry; unprocessed artificial resins, unprocessed plastics; </span><span class="tx">manures; fire extinguishing compositions; tempering and soldering preparations; </span><span class="tx">chemical substances for preserving foodstuffs; tanning substances; adhesives used in </span><span class="tx">industry; unprocessed plastics in the form of liquids, chips or granules.</span>
<br><br>
<strong><span class="tx f132">Class 2.  Paint Products</span></strong>
<br>
<span class="tx">Paints, varnishes, lacquers; preservatives against rust and against deterioration of </span><span class="tx">wood; colorants; mordants; raw natural resins; metals in foil and powder form for</span><span class="tx">painters, decorators, printers and artists.</span>
<br><br>
<strong><span class="tx f132">Class 3.  Cosmetics and Cleaning Products</span></strong>
<br>
<span class="tx">Bleaching preparations and other substances for laundry use; cleaning, polishing, </span><span class="tx">scouring and abrasive preparations; soaps; perfumery, essential oils, cosmetics, hair</span><span class="tx">lotions; dentifrices.</span>
<br><br>
<strong><span class="tx f132">Class 4.  Industrial Oils, Greases and Fuels</span></strong>
<br>
<span class="tx">Industrial oils and greases; lubricants; dust absorbing, wetting and binding </span><span class="tx">compositions; fuels and illuminants; candles and wicks for lighting; combustible </span><span class="tx">fuels, electricity and scented candles.</span>
<br><br>
<strong><span class="tx f132">Class 5.  Pharmaceutical and Veterinary Products</span></strong>
<br>
<span class="tx">Pharmaceutical and veterinary preparations; sanitary preparations for medical </span><span class="tx">purposes; dietetic food and substances adapted for medical or veterinary use, food </span><span class="tx">for babies; dietary supplements for humans and animals; plasters, materials for </span><span class="tx">dressings; material for stopping teeth, dental wax; disinfectants; preparations for </span><span class="tx">destroying vermin; fungicides, herbicides.</span>
<br><br>

<strong><span class="tx f132">Class 6. Metal Products</span></strong>
<br>
<span class="tx">Common metals and their alloys; metal building materials; transportable buildings of metal;
materials of metal for railway tracks; non-electric cables and wires of common metal; ironmongery,
small items of metal hardware; pipes and tubes of metal; safes; goods of common
metal not included in other classes; ores; unwrought and partly wrought common metals;
metallic windows and doors; metallic framed conservatories.</span>
<br><br>


<strong><span class="tx f132">Class 7. Machinery Products</span></strong>
<br>
<span class="tx">Machines and machine tools; motors and engines (except for land vehicles); machine coupling
and transmission components (except for land vehicles); agricultural implements other
than hand-operated; incubators for eggs; automatic vending machines.</span>
<br><br>

<strong><span class="tx f132">Class 8. Hand Tool Products</span></strong>
<br>
<span class="tx">Hand tools and hand operated implements; cutlery; side arms; razors; electric razors and hair
cutters.</span>
<br><br>

<strong><span class="tx f132">Class 9. Computer, Electrical and Scientific Products</span></strong>
<br>
<span class="tx">Scientific, nautical, surveying, photographic, cinematographic, optical, weighing, measuring,
signalling, checking (supervision), life-saving and teaching apparatus and instruments; apparatus
and instruments for conducting, switching, transforming, accumulating, regulating or
controlling electricity; apparatus for recording, transmission or reproduction of sound or images;
magnetic data carriers, recording discs; compact discs, DVDs and other digital recording
media; mechanisms for coin-operated apparatus; cash registers, calculating machines,
data processing equipment, computers; computer software; fire-extinguishing apparatus.</span>
<br><br>

<strong><span class="tx f132">Class 10. Medical Instrumental Products</span></strong>
<br>
<span class="tx">Surgical, medical, dental and veterinary apparatus and instruments, artificial limbs, eyes and
teeth; orthopaedic articles; suture materials; sex aids; massage apparatus; supportive bandages;
furniture adapted for medical use.</span>
<br><br>

<strong><span class="tx f132">Class 11. Environmental Controls and Appliances</span></strong>
<br>
<span class="tx">Apparatus for lighting, heating, steam generating, cooking, refrigerating, drying, ventilating,
water supply and sanitary purposes; air conditioning apparatus; electric kettles; gas and electric
cookers; vehicle lights and vehicle air conditioning units.</span>
<br><br>

<strong><span class="tx f132">Class 12. Vehicles and Locomotives</span></strong>
<br>
<span class="tx">Vehicles; apparatus for locomotion by land, air or water; wheelchairs; motors and engines for
land vehicles; vehicle body parts and transmissions.</span>
<br><br>

<strong><span class="tx f132">Class 13. Firearms and Fireworks Products</span></strong>
<br>
<span class="tx">Firearms; ammunition and projectiles, explosives; fireworks.</span>
<br><br>

<strong><span class="tx f132">Class 14. Jewellery Products</span></strong>
<br>
<span class="tx">Precious metals and their alloys; jewellery, costume jewellery, precious stones; horological
and chronometric instruments, clocks and watches.</span>
<br><br>

<strong><span class="tx f132">Class 15. Musical Instruments</span></strong>
<br>
<span class="tx">Musical instruments; stands and cases adapted for musical instruments.</span>
<br><br>



<strong><span class="tx f132">Class 16. Paper Goods and Printed Materials</span></strong>
<br>
<span class="tx">Paper, cardboard and goods made from these materials, not included in other classes; printed
matter; bookbinding material; photographs; stationery; adhesives for stationery or household
purposes; artists’ materials; paint brushes; typewriters and office requisites (except furniture);
instructional and teaching material (except apparatus); plastic materials for packaging
(not included in other classes); printers’ type; printing blocks.</span>
<br><br>



<strong><span class="tx f132">Class 17. Rubber and Plastic Products</span></strong>
<br>
<span class="tx">Rubber, gutta-percha, gum, asbestos, mica and goods made from these materials; plastics
in extruded form for use in manufacture; semi-finished plastics materials for use in further
manufacture; stopping and insulating materials; flexible non-metallic pipes.</span>
<br><br>



<strong><span class="tx f132">Class 18. Leather Products</span></strong>
<br>
<span class="tx">Leather and imitations of leather; animal skins, hides; trunks and travelling bags; handbags,
rucksacks, purses; umbrellas, parasols and walking sticks; whips, harness and saddlery; clothing
for animals.</span>
<br><br>



<strong><span class="tx f132">Class 19. Non Metallic Building Material Products</span></strong>
<br>
<span class="tx">Non-metallic building materials; non-metallic rigid pipes for building; asphalt, pitch and bitumen;
non-metallic transportable buildings; non-metallic monuments; non-metallic framed
conservatories, doors and windows.</span>
<br><br>


<strong><span class="tx f132">Class 20. Furniture Products</span></strong>
<br>
<span class="tx">Furniture, mirrors, picture frames; articles made of wood, cork, reed, cane, wicker, horn,
bone, ivory, whalebone, shell, amber, mother-of-pearl, meerschaum or plastic which are not
included in other classes; garden furniture; pillows and cushions.</span>
<br><br>


<strong><span class="tx f132">Class 21. Houseware and Glass Products</span></strong>
<br>
<span class="tx">Household or kitchen utensils and containers; combs and sponges; brushes (except paintbrushes);
brush-making materials; articles for cleaning purposes; steel wool; articles made
of ceramics, glass, porcelain or earthenware which are not included in other classes; electric
and non-electric toothbrushes.</span>
<br><br>



<strong><span class="tx f132">Class 22. Ropes and Fibre Products</span></strong>
<br>
<span class="tx">Ropes, string, nets, tents, awnings, tarpaulins, sails, sacks for transporting bulk materials; padding
and stuffing materials which are not made of rubber or plastics; raw fibrous textile materials.</span>
<br><br>


<strong><span class="tx f132">Class 23. Yarns and Threads</span></strong>
<br>
<span class="tx">Yarns and threads, for textile use.</span>
<br><br>


<strong><span class="tx f132">Class 24. Fabrics</span></strong>
<br>
<span class="tx">Textiles and textile goods; bed and table covers; travellers’ rugs, textiles for making articles of
clothing; duvets; covers for pillows, cushions or duvets.</span>
<br><br>


<strong><span class="tx f132">Class 25. Clothing, Footwear and Headgear Products</span></strong>
<br>
<span class="tx">Clothing, footwear, headgear.</span>
<br><br>

<strong><span class="tx f132">Class 26. Ribbons, Buttons and Pins</span></strong>
<br>
<span class="tx">Lace and embroidery, ribbons and braid; buttons, hooks and eyes, pins and needles; artificial
flowers.</span>
<br><br>

<strong><span class="tx f132">Class 27. Floor & Wall Covering Products</span></strong>
<br>
<span class="tx">Carpets, rugs, mats and matting, linoleum and other materials for covering existing floors;
wall hangings (non-textile); wallpaper.</span>
<br><br>

<strong><span class="tx f132">Class 28. Toys and Sporting Goods Products</span></strong>
<br>
<span class="tx">Games and playthings; playing cards; gymnastic and sporting articles; decorations for Christmas
trees; children’s toy bicycles.</span>
<br><br>

<strong><span class="tx f132">Class 29. Meats and Processed Foods</span></strong>
<br>
<span class="tx">Meat, fish, poultry and game; meat extracts; preserved, dried and cooked fruits and vegetables;
jellies, jams, compotes; eggs, milk and milk products; edible oils and fats; prepared
meals; soups and potato crisps.</span>
<br><br>

<strong><span class="tx f132">Class 30. Coffee and Staple Food Products</span></strong>
<br>
<span class="tx">Coffee, tea, cocoa, sugar, rice, tapioca, sago, artificial coffee; flour and preparations made
from cereals, bread, pastry and confectionery, edible ices; honey, treacle; yeast, baking-powder;
salt, mustard; vinegar, sauces (condiments); spices; ice; sandwiches; prepared meals;
pizzas, pies and pasta dishes.</span>
<br><br>

<strong><span class="tx f132">Class 31. Natural Agricultural Products</span></strong>
<br>
<span class="tx">Agricultural, horticultural and forestry products; live animals; fresh fruits and vegetables,
seeds, natural plants and flowers; foodstuffs for animals; malt; food and beverages for animals.</span>
<br><br>

<strong><span class="tx f132">Class 32. Soft Drinks and Beer Products</span></strong>
<br>
<span class="tx">Beers; mineral and aerated waters; non-alcoholic drinks; fruit drinks and fruit juices; syrups for
making beverages; shandy, de-alcoholised drinks, non-alcoholic beers and wines.</span>
<br><br>

<strong><span class="tx f132">Class 33. Wines and Spirits Products</span></strong>
<br>
<span class="tx">Alcoholic beverages (except beers); alcoholic wines; spirits and liqueurs; alcopops; alcoholic
cocktails.</span>
<br><br>

<strong><span class="tx f132">Class 34. Smokers Products</span></strong>
<br>
<span class="tx">Tobacco; smokers’ articles; matches; lighters for smokers.
Classification of services (class 35 - 45)</span>
<br><br>

<strong><span class="tx f132">Class 35. Advertising and Business Services</span></strong>
<br>
<span class="tx">Advertising; business management; business administration; office functions; organisation,
operation and supervision of loyalty and incentive schemes; advertising services provided via
the Internet; production of television and radio advertisements; accountancy; auctioneering;
trade fairs; opinion polling; data processing; provision of business information; retail services
connected with the sale of [list specific goods].</span>
<br><br>

<strong><span class="tx f132">Class 36. Financial and Estate Agency Services</span></strong>
<br>
<span class="tx">Insurance; financial services; real estate agency services; building society services; banking;
stockbroking; financial services provided via the Internet; issuing of tokens of value in relation
to bonus and loyalty schemes; provision of financial information.</span>
<br><br>

<strong><span class="tx f132">Class 37. Construction and Repair Services</span></strong>
<br>
<span class="tx">Building construction; repair; installation services; installation, maintenance and repair of
computer hardware; painting and decorating; cleaning services.</span>
<br><br>

<strong><span class="tx f132">Class 38. Communication Services</span></strong>
<br>
<span class="tx">Telecommunications services; chat room services; portal services; e-mail services; providing
user access to the Internet; radio and television broadcasting.</span>
<br><br>

<strong><span class="tx f132">Class 39. Transportation and Storage Services</span></strong>
<br>
<span class="tx">Transport; packaging and storage of goods; travel arrangement; distribution of electricity;
travel information; provision of car parking facilities.</span>
<br><br>

<strong><span class="tx f132">Class 40. Printing Services and Treatment of Materials Services</span></strong>
<br>
<span class="tx">Treatment of materials; development, duplicating and printing of photographs; generation of
electricity.</span>
<br><br>

<strong><span class="tx f132">Class 41. Education, Sport and Culture Services</span></strong>
<br>
<span class="tx">Education; providing of training; entertainment; sporting and cultural activities.</span>
<br><br>

<strong><span class="tx f132">Class 42. Scientific, Computer and Software Services</span></strong>
<br>
<span class="tx">Scientific and technological services and research and design relating thereto; industrial
analysis and research services; design and development of computer hardware and software;
computer programming; installation, maintenance and repair of computer software; computer
consultancy services; design, drawing and commissioned writing for the compilation of
web sites; creating, maintaining and hosting the web sites of others; design services.</span>
<br><br>

<strong><span class="tx f132">Class 43. Restaurant and Hospitality Services</span></strong>
<br>
<span class="tx">Services for providing food and drink; temporary accommodation; restaurant, bar and catering
services; provision of holiday accommodation; booking and reservation services for
restaurants and holiday accommodation; retirement home services; creche services.</span>
<br><br>

<strong><span class="tx f132">Class 44. Medical, Agricultural and Beauty Services</span></strong>
<br>
<span class="tx">Medical services; veterinary services; hygienic and beauty care for human beings or animals;
agriculture, horticulture and forestry services; dentistry services; medical analysis for the diagnosis
and treatment of persons; pharmacy advice; garden design services.</span>
<br><br>

<strong><span class="tx f132">Class 45. Personal & Legal and Social Services</span></strong>
<br>
<span class="tx">Legal services; conveyancing services; security services for the protection of property and individuals;
social work services; consultancy services relating to health and safety; consultancy
services relating to personal appearance; provision of personal tarot readings; dating services;
funeral services and undertaking services; fire-fighting services; detective agency services.</span>
<br><br>


<a href="{{ asset('pdf/Classification of Goods & Services.pdf') }}" class="cp-green" title="download pdf">Download PDF</a>

           </div>
        </div>
    </div>
</div>
@endsection