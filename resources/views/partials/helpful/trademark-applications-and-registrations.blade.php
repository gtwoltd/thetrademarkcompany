@section('title')
{{"The Trademark Company"}}
@stop


@section('description')
{{ ""}}
@stop

@extends('index')

@section('content')

<div class="content-section-a" style="margin-top:-30px" itemscope itemtype="http://schema.org/Question">

    <div class="container">
        <div class="row">
            <div class="line col-md-12 col-sm-12">


<h3><strong>Trademark applications and registration</strong></h3>
<br>
<strong><span class="tx f119" itemprop="name">How can I find out if my trademark can be registered?</span></strong>
<br><br>
<div itemprop="text">
<span class="tx">We recommend that before you apply to register your trademark you search on our </span><span class="tx">Free search to see if there could be any reasons for it not being approved.  We </span><span class="tx">recommend this because your application could be rejected and therefore cost you </span><span class="tx">time and money.</span>
<br><br>
<span class="tx">Not only do we offer you a free initial check, which many other firms out there </span><span class="tx">charge for, we then carry out further legal checks until we are satisfied and only then </span><span class="tx">will we submit your application to the examiner.  </span>
</div>
<br><br>


<strong><span class="tx f119" itemprop="name">How long does the process of registering a trademark take?</span></strong>
<br><br>
<div itemprop="text">
<span class="tx">In the UK, the process usually takes between four to six months. This is only if the </span><span class="tx">application is not challenged by another trademark owner.  If your application is </span><span class="tx">opposed then it is likely that it can take up to a further 6 months to complete.</span>

<br><br>
<span class="tx">Applications for EU trademark registrations is slightly longer and dependent upon </span><span class="tx">no oppositions it will usually take nine to twelve months to complete.  If opposed </span><span class="tx">then in our experience it will extend this process by another three to six months.</span>
</div>
<br><br>

<strong><span class="tx f119" itemprop="name">Is my registration guaranteed if my application passes your checks? </span></strong>
<br><br>
<div itemprop="text">
<span class="tx">We do not submit any applications or process your application fee unless our experts </span><span class="tx">believe that your application will be successful and that there will be no grounds for </span><span class="tx">it to be challenged by registered trademark owners.</span>
<br><br>
<span class="tx">Once submitted to the examiners, we cannot guarantee the success of your </span><span class="tx">application however we are so confident in our process that we offer a money back </span><span class="tx">guarantee in the unlikely event that your application is rejected.</span>
</div>
<br><br>

<strong><span class="tx f119" itemprop="name">Why Should I register in the UK or across the EU?</span></strong>
<br><br>
<div itemprop="text">
<span class="tx">For protection of your goods and services in all European Union Countries (and not </span><span class="tx">just the UK) then you need a Community Trademark (CTM), which provides </span><span class="tx">protection in all member states of the EU. It is a single application process and works </span><span class="tx">out at a much lower cost than filing in each country separately. Those who hold a </span><span class="tx">CTM have priority over an application for a similar mark in just the UK.</span>
<br><br>
<span class="tx">If you are planning on operating only in Britain, then a UK registered trademark is </span><span class="tx">granted faster and more cheaply.</span>
</div>
<br><br>


<span class="tx"><strong itemprop="name">How does the UK registration process work?</strong></span>
<br><br>

<div itemprop="text">

<span class="tx">A brief explanation of the UK trademark registration process:</span>
<br><br>

<span class="tx">
	<ol>
		<li>You apply online via The Trademark Company website;</li>
		<li>Our trademark experts perform legal checks on your application;</li>
		<li>If your application passes our checks, we file your application with the registry;</li>
		<li>The Intellectual Property Office (IPO) confirms receipt of your trademark application within six days;</li>
		<li>An IPO examiner reports on your application within two months;</li>
		<li> If your application is accepted, your details are published in the trademark journal within three weeks;</li>
		<li>After three weeks, if no oppositions are registered, your registration certificate is issued.</li>
	</ol>
</span>
</div>
<br><br>

<span class="tx"><strong itemprop="name">How does the EU (CTM) registration process work?</strong></span>
<br><br>

<div itemprop="text">
<span class="tx">The EU (CTM) trademark registration process in brief:</span>
<br><br>

<span class="tx">
	<ol>
		<li>You apply online via The Trademark Company website;</li>
		<li>Our trademark experts perform legal checks on your application;</li>
		<li>If your application passes our checks, we file your application with the registry;</li>
		<li>Your trademark application is examined by the Office for Harmonization in the Internal Market (OHIM) within six to nine months, some national registry reports are sent;</li>
		<li>If your trademark application is accepted on absolute grounds, your details are published in the Community Trade Marks bulletin within two months;</li>
		<li>After three months, if no oppositions arise, your registration certificate is issued.</li>
	</ol>
</span>
</div>
<br><br>

<span class="tx"><strong itemprop="name">How do I register a logo as a trademark?</strong></span>
<br><br>

<div itemprop="text">
<span class="tx">To register a logo trademark, the application will need to pass additional criteria before it is
able to be registered. The logo trademark is unable to be:</span>
<br><br>

<span class="tx">
	
	<ul>
		<li> A three dimensional shape that represents typical goods you sell, has a function or adds
value to those goods;</li>
		<li> A specially protected emblem, such as flags, royal insignia and the rings of the Olympic
Games;</li>
		<li> A mark that is deceptive as to the country of the product’s origin; Obscene.</li>
	</ul>
</span>

<span class="tx">Logo trademark applications require an additional £75 charge, but are submitted in the same
manner as text trademarks.</span>
</div>
<br><br>

<span class="tx"><strong itemprop="name">What is better to register, a logo trademark or a text trademark?</strong></span>
<br><br>
<div itemprop="text">
<span class="tx">A text trademark provides you with wider and stronger rights than a logo trademark, as with
a logo trademark your rights are limited to just the combined words and graphical elements.
Therefore, there is the potential for someone to register the same words with a logo
trademark but just with a different design. Where on the other hand, with a text trademark no
one can use those words, with or without a design. For this reason, wherever we can, we try
to register a text trademark. </span>
<br><br>

<span class="tx">If you decide to register both the words and then separately the logo, the registries treat this
as two applications and therefore two sets of fees have to be paid. As standard, we charge
two sets of our advice fees, but we can be flexible.</span>
<br><br>
</div>

<span class="tx"><strong itemprop="name">Can I register a trademark without having launched a business?</strong></span>
<br><br>
<div itemprop="text">
<span class="tx">Anyone is able to apply to register a trademark. However, if your application is similar to
another registered trademark and you are not the proprietor of a business, it can be opposed
on the basis that the application was made in bad faith.</span>
</div>
<br><br>

<span class="tx"><strong itemprop="name">If there are objections from the registries, what happens?</strong></span>
<br><br>

<div itemprop="text">
<span class="tx">Trademark applications can be objected by trademark registries on absolute groups. As the
applicant you are able to put forward counter-arguments. If these counter-arguments are still
not accepted by the registry, there is a subsequent appeals process. </span>
<br><br>

<span class="tx">Similarly is also checked by the UK registry. You as the applicant will be notified of any similar
marks, and the holder of those marks are also informed of the application. The holders of
the existing trademarks are then able to oppose the application if they choose to do so. It
is important to note however that the registry will not reject an application simply due to
similarly, unless opposition is then encountered and upheld. </span>
<br>

<span class="tx"><strong>Important</strong>: The Trademark Company does not put forward applications to registries unless
we completely believe that they will succeed. We currently have a 98% success rate on our
guaranteed applications. 
</span>
<br>

<span class="tx"><strong>Please note</strong>: We do not support applicants in overcoming objections. If you wish to pursue
your application we are able to recommend experienced trademark agents, however you
will have to pay additional fees. If you choose to not pursue the application to the full extent
possible, all rights under our guarantee scheme lapse, so you will not be reimbursed our for
our fees or statutory fees.</span>
<br><br>

<a href="{{ asset('pdf/Trademark Applications and Registration.pdf') }}" class="cp-green">Download PDF</a>
</div>

           </div>
        </div>
    </div>
</div>
@endsection