@section('title')
{{"The Trademark Company"}}
@stop


@section('description')
{{ ""}}
@stop

@extends('index')

@section('content')

<div class="content-section-a" style="margin-top:-30px">

    <div class="container">
        <div class="row">
            <div class="line col-md-12 col-sm-12">
<div itemscope itemtye="https://schema.org/CommunicateAction">

<div itemprop="about">
<h3><strong><span class="tx">What is a trademark?</span></strong></h3>
<br>
<span class="tx">A trademark is a recognisable sign, design or expression which identifies your goods </span><span class="tx">and services from those of any other businesses. Your trademark is also described as </span><span class="tx">your ‘brand’ and can be made up of words, logos or a combination of both.  A trademark</span><span class="tx"> may be located on a package, a label, a voucher or on the product itself.</span>
<br><br>
<span class="tx">What trademark classification?</span>
<br><br>
<span class="tx">“This is a system that is divided between Goods and Services and you must select </span><span class="tx">those that are best suited to your business.  For you to register your trademark, you </span><span class="tx">have to register in one or more of the 45 classes. There is no set rule as to whether </span><span class="tx">you register with one or several classes.</span>
<br><br>
<span class="tx">Dependent upon the number of different classes you register your trademark </span><span class="tx">application will reflect the fees that you pay. Once you have made your application </span><span class="tx">to The Trademark Company, we can research into whether you have selected the </span><span class="tx">relevant classes to ensure that your brand is fully protected once your registration is </span><span class="tx">complete.</span>
<br><br>
</div>
</div>

<span class="tx"><strong>Does everyone need a trademark?</strong></span>
<br><br>

<span class="tx">Every company has a trademark. You cannot trade without one. How else would your
existing customers find you again and how would new customers find you in the first place?</span>
<br><br>

<span class="tx">Some trademarks are so valuable, it is near impossible to put a price on them. How much
would someone pay to be the only one allowed to use Coca Cola for fizzy drinks? Or
Microsoft for computers?</span>
<br><br>

<span class="tx">Most trademarks, of course, are not as valuable as Coca Cola or Microsoft, but, still, your
trademark is an asset that should be protected against potential misuse by others.</span>
<br><br>

<span class="tx">If somebody else used your trademark (or even a similar mark), would your customers (or
potential new customers) be confused? What impact would that have on your business?</span>
<br><br>

<span class="tx">Could you justify just one sale being lost because someone has bought goods from another
company thinking that they are your goods, or if you are a service provider and you lose out
on work that should have come to you because one of your competitors has used your mark
or something similar, that is already too much lost business. If confusion arises, you will not
lose just the one sale.</span>
<br><br>

<span class="tx"><strong>I have registered with Companies House so am I covered?</strong></span>
<br><br>

<span class="tx">No, simply registering your business with Companies House will not offer your trademark
any protection under the law. The only way to protect your brand and your business is if you
register your trademark.</span>
<br><br>

<span class="tx"><strong>What is involved with registering a trademark?</strong></span>
<br><br>

<span class="tx">To be a legally binding trademark, the mark must be distinctive for the goods and services for
which you want to supply. A registered trademark will allow customers to distinguish your
goods or service from another company’s.</span>
<br><br>

<span class="tx"><strong>Can an existing word be registered as a trademark?</strong></span>
<br><br>

<span class="tx">Yes, on the condition the existing word:</span>
<br>
<span class="tx">
	<ul>
		<li>Distinguishes your business from others</li>
		<li>Doesn’t describe your services or goods</li>
		<li> Is not already registered</li>
		<li>Is not similar to a registered trademark of another business working in
your line of trade</li>
		<li> Is not seen to be offensive</li>
		<li>Will not attempt to mislead the public</li>
		<li> Does not promote any illegal activities;</li>
	</ul>

</span>
<br><br>

<span class="tx"><strong>Can two businesses own the same trademark?</strong></span>
<br><br>

<span class="tx">Yes, two businesses are permitted to register the same trademark providing their areas of
trade don’t overlap, e.g. Doves Dentist Surgery and Doves Electrical Supplies.</span>
<br><br>

<span class="tx"><strong>Can I use another company’s registered trademark?</strong></span>
<br><br>

<span class="tx">By using a trademark of a company that is the same or similar in your line of business, you
may well be subject to legal action or even criminal charges. Even if there may be slight
differences in how your business operates. When registering a trademark they need to be
registered under different ‘classes’, i.e. good and services and by law your business activities
may well overstep on their registered trademark. The only way to guarantee that your use of
a trademark does not infringe trademark law is to register it with the proper authorities.</span>
<br><br>





<a href="{{ asset('pdf/About Trademarks.pdf') }}" class="cp-green">Download PDF</a>

           </div>
        </div>
    </div>
</div>
@endsection