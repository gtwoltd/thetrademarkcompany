@section('title')
{{"Thank You | The Trademark Company"}}
@stop


@section('description')
{{ ""}}
@stop


@extends('index')

@section('tagmanager')
<?php $num = rand(500, 10000); ?>
    <script>
        ga('require', 'ecommerce');

        ga('ecommerce:addTransaction', {
          'id': '{{$num}}',                     // Transaction ID. Required.
          'affiliation': 'The Trademark Company',   // Affiliation or store name.
          'revenue': '699.00',               // Grand Total.
          'shipping': '0',                  // Shipping.
          'tax': '0'                     // Tax.
        });

        ga('ecommerce:addItem', {
          'id': '{{$num}}',                     // Transaction ID. Required.
          'name': 'UK Full Clearance Trademark Package',    // Product name. Required.
          'sku': '',                 // SKU/code.
          'category': 'The Trademark Company',         // Category or variation.
          'price': '699.00',                 // Unit price.
          'quantity': '1'                   // Quantity.
        });

        ga('ecommerce:send');
    </script>
@endsection

@section('content')
<div class="content-section-a" style="margin-top:-30px">

    <div class="container">
        <div class="row">
            <div class="line col-md-12 col-sm-12">
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>

            <center>
            	<img src="{{ asset('assets/img/logo-sm.png') }}" />
            	 <p>&nbsp;</p>
            	<h1>Thank You</h1>
            	<p>Thanks for your order <strong>UK Full Clearance</strong>. We will contact you shortly.</p>
            </center>

            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>


            </div>
        </div>
    </div>
</div>
@endsection