@section('title')
{{"Terms and Conditions | The Trademark Company"}}
@stop


@section('description')
{{ ""}}
@stop


 @extends('index')

@section('content')
    <div class="content-section-a" style="margin-top:-30px">

        <div class="container">
            <div class="row">
                <div class="line col-md-12 col-sm-12">
                    <h2 class="section-heading text-center cp-main cp-strong">Terms and Conditions</h2>

 <div class="col-xs-12 tac-text">
            <p>This page (together with the documents expressly referred to in it) tells you information about us and the legal terms and conditions (<b>Terms</b>) on which we supply the documents described herein (<b>Documents</b>) to you.</p>

            <p>
            </p>

            <p>These Terms will apply to any contract between us for the supply of the Documents to you (<b>Contract</b>). Please read these Terms carefully and make sure that you understand them, before ordering the Documents from our website (<b>our Site</b>). Please note that by setting up or using an Account, you agree to be bound by these Terms and the other documents expressly referred to in them.</p>

            <p>
            </p>

            <p>If you refuse to accept these Terms, you will be unable to register an Account and you will be unable to order the Documents.</p>

            <p>
            </p>

            <p>You should print a copy of these Terms or save them to your computer for future reference.</p>

            <p>We amend these Terms from time to time as set out in clause 8. Every time you wish to order the Documents, please check these Terms to ensure you understand the terms which will apply at that time.</p>

            <p>
            </p>
            
            <div class="section-number">1. INTERPRETATIONS AND DEFINITIONS</div>
            
            <div class="section-content">
                <p>The definitions and rules of interpretation in this clause 1.1 apply in this Agreement:</p>

                <p><b>Agreement</b>: collectively these terms and conditions, notices on our Site and the Order Form</p>

                <p><b>Delivery</b>: delivery shall be made the instant you press "submit" or similar at the end of Will Wizard in relation to a Document.</p>

                <p><b>Documents</b>: documents produced via your input of information into the Will Wizard for your personal use.</p>

                <p><b>Fact Find</b>: interactive pages on our Site which will capture the information you input and insert that information into the Document(s).</p>

                <p><b>Order Form</b>: the form on our Site that enables you to select a Document, input your customer details and complete the order. The contract is formed upon your completion and submission of the Order Form.</p>

                <p><b>Service or Services</b>: the collection of your information as input by you and transfer of it into the Document(s) you have selected and the production of the Document(s), storage of Documents and any other services that we may provide from time to time. </p>

                <ul class="sub-sections-in-content"> 
                    <li><p><span>1.1</span> Clause headings do not affect the interpretation of this agreement.</p></li>
                    <li><p><span>1.2</span> A reference to a person includes a natural person, a corporate or unincorporated body (whether or not having a separate legal personality).</p></li>
                    <li><p><span>1.3</span> A reference to a law is a reference to it as it is in force for the time being taking account of any amendment, extension,  application or re-enactment and includes any subordinate legislation for the time being in force made under it.</p></li>
                    <li><p><span>1.4</span> A reference to writing or written includes faxes.</p></li>
                    <li><p><span>1.5</span> Unless the context otherwise requires, words in the singular shall include the plural and in the plural shall include the singular and a reference to one gender shall include a reference to the other genders.</p></li>
                </ul>
            </div>

            <div class="section-number">2. TERMS AND CONDITIONS OF USE</div>
            
            <div class="section-content">
                <ul class="sub-sections-in-content"> 
                    <li><p><span>2.1</span> You must indicate that you accept these Terms by ticking the box "I accept" on the Order Form at the end of the Will Wizard, at which point the Agreement between us comes into force. If you do not read these Terms and tick the acceptance box, you will not be able to use the Services.</p></li>
                    <li><p><span>2.2</span> You must follow the instructions given to you on the Site for the completion of your Document(s), and you must then review, print and sign your documents in front of witnesses as directed. You are responsible for any failure to properly sign the Document(s).</p></li>
                </ul>
            </div>

            <div class="section-number">3. LIMITED WARRANTY</div>
            <div class="section-content">
                <ul class="sub-sections-in-content"> 
                    <li>
                        <p><span>3.1</span> We will use our reasonable endeavours to: </p>
                        <ul class="letters-in-sections-in-content">
                            <li>maintain the website and Services with due care and skill,</li>
                            <li>ensure the Documents are reasonably fit for their stated purpose, and</li>
                            <li>review changes in the law and revise the Services accordingly.</li>
                        </ul>
                        
                    </li>
                    <li><p><span>3.2</span> Should you encounter any problems using the Will Wizard then you may contact us using the 'Contact Us' link on the website. We will use reasonable endeavours to respond to your query so as to assist you in completing the Will Wizard but we will not offer legal or financial advice.</p></li>
                </ul>
            </div>
            
            <div class="section-number">4.LIMITATION &amp; EXCLUSION OF LIABILITY</div>

            <div class="section-content">
                <ul class="sub-sections-in-content"> 
                    <li>
                        <p><span>4.1</span> We only supply the Products for domestic and private use. You agree not to use the product for any commercial, business or resale purposes, and we have no liability to you for any loss of profit, loss of business, business interruption, or loss of business opportunity.</p>
                    </li>
                    <li>
                        <p><span>4.2</span> Nothing on our website constitutes legal or financial advice and you should seek independent professional advice before using the Services if you are unsure about your circumstances and/or the content of the Services/Documents.</p>
                    </li>
                    <li>
                        <p><span>4.3</span> The laws of England and Wales apply to the content of the Services and if you live or have assets outside the jurisdiction you should take independent professional advice before using the Services. If you continue to use our Services and produce a Document, we will not accept liability for issues arising in connection with jurisdiction nor process a refund.</p>
                    </li>
                    <li>
                        <p><span>4.4</span> Legislation and case law may affect the content of the Will Wizard and the Documents; we will update accordingly but cannot guarantee that the content is up to date, complete or accurate at the point of your use.</p>
                    </li>
                    <li>
                        <p><span>4.5</span> If we fail to comply with these Terms, we are responsible for loss or damage you suffer that is a foreseeable result of our breach of these Terms or our negligence, but we are not responsible for any loss or damage that is not foreseeable. Loss or damage is foreseeable if it is an obvious consequence of our breach or if it was contemplated by you and us at the time we entered into this contract.</p>
                    </li>
                    <li>
                        <p><span>4.6</span> Neither we nor our suppliers or affiliates will be liable to you in contract, tort (including negligence) or otherwise for any indirect, consequential, special or incidental damage or loss arising from your use of or inability to use Services, including (without limitation) loss of business or profits or anticipated savings, loss or corruption of data, loss caused by a virus, loss of or damage to property, claims of third parties, fines or penalties levied by any taxing or other authority or any other loss or damage. In particular, neither we nor our suppliers or affiliates will be liable for any Inheritance Tax liability incurred by your Estate as a result of the interpretation of your Will by the appropriate Tax authorities.</p>
                    </li>
                    <li>
                        <p><span>4.7</span> Notwithstanding the foregoing, our liability to you in respect of losses or damages arising directly or indirectly from your use of the Services howsoever arising shall be limited, in relation to any one incident or series of related incidents, to the lesser of (i) 100% of the amount paid by you for the Legal Document, or (ii) £1000.</p>
                    </li>
                    <li>
                        <p><span>4.8</span> Nothing in this Agreement limits our liability in relation to death or personal injury caused by our negligence.</p>
                    </li>
                </ul>
            </div>
            
            <div class="section-number">5.USE OF OUR SERVICES</div>

            <div class="section-content">
                <ul class="sub-sections-in-content"> 
                    <li>
                        <p><span>5.1</span> Accessing our site</p>
                        <ul class="letters-in-sections-in-content">
                            <li>We do not guarantee that our Site, or any content on it, will always be available or be uninterrupted. Access to our Site is permitted on a temporary basis.</li>
                            <li>We may suspend, withdraw, discontinue or change all or any part of our site without notice. We will not be liable to you if for any reason our Site is unavailable at any time or for any period.</li>
                            <li>You are responsible for making all arrangements necessary for you to have access to our site.</li>
                            <li>You are also responsible for ensuring that all persons who access our site through your internet connection are aware of these terms of use and other applicable terms and conditions, and that they comply with them.</li>
                        </ul>
                        
                    </li>
                    <li>
                        <p><span>5.2</span> Your account and password</p>
                        <ul class="letters-in-sections-in-content">
                            <li>If you choose, or you are provided with, a user identification code, password or any other piece of information as part of our security procedures, you must treat such information as confidential. You must not disclose it to any third party.</li>
                            <li>We have the right to disable any user identification code or password, whether chosen by you or allocated by us, at any time, if in our reasonable opinion you have failed to comply with any of the provisions of these Terms.</li>
                            <li>If you know or suspect that anyone other than you knows your user identification code or password, you must promptly notify us at info@thetrademarkcompany.co.uk.</li>
                        </ul>
                    </li>
                    <li>
                        <p><span>5.3</span> Changes to our site</p>
                        <ul class="letters-in-sections-in-content">
                            <li>We may update our Site from time to time, and may change the content at any time. However, please note that any of the content on our site may be out of date at any given time, and we are under no obligation to update it.</li>
                            <li>We do not guarantee that our site, or any content on it, will be free from errors or omissions.</li>
                        </ul>
                        
                    </li>
                    <li>
                        <p><span>5.4</span> Intellectual property rights</p>
                        <ul class="letters-in-sections-in-content">
                            <li>We are the owner or the licensee of all intellectual property rights in our site, and in the material published on it. Those works are protected by copyright laws and treaties around the world. All such rights are reserved.</li>
                            <li>You may print off one copy, and may download extracts, of any page(s) from our site for your personal use and you may print off as many copies of a Document produced via the Services as required for your use.</li>
                            <li>Our status (and that of any identified contributors) as the authors of content on our site must always be acknowledged.</li>
                            <li>You must not use any part of the content on our site for commercial purposes without obtaining a licence to do so from us.</li>
                            <li>If you print off, copy or download any part of our site in breach of these Terms, your right to use our site will cease immediately and you must, at our option, return or destroy any copies of the materials you have made.</li>
                        </ul>
                    </li>
                    <li>
                        <p><span>5.5</span> No reliance on information</p>
                        <ul class="letters-in-sections-in-content">
                            <li>The content on our site is provided for general information only. It is not intended to amount to advice on which you should rely. You must obtain professional or specialist advice before taking, or refraining from, any action on the basis of the content on our Site.</li>
                            <li>Although we make reasonable efforts to update the information on our site, we make no representations, warranties or guarantees, whether express or implied, that the content on our site is accurate, complete or up-to-date.</li>
                        </ul>
                    </li>
                    <li>
                        <p><span>5.6</span> Viruses</p>
                        <ul class="letters-in-sections-in-content">
                            <li>We do not guarantee that our site will be secure or free from bugs or viruses.</li>
                            <li>You are responsible for configuring your information technology, computer programmes and platform in order to access our site. You should use your own virus protection Documents.</li>
                            <li>You must not misuse our site by knowingly introducing viruses, trojans, worms, logic bombs or other material which is malicious or technologically harmful. You must not attempt to gain unauthorised access to our site, the server on which our site is stored or any server, computer or database connected to our site. You must not attack our site via a denial-of-service attack or a distributed denial-of service attack. By breaching this provision, you would commit a criminal offence under the Computer Misuse Act 1990. We will report any such breach to the relevant law enforcement authorities and we will co-operate with those authorities by disclosing your identity to them. In the event of such a breach, your right to use our site will cease immediately.</li>
                        </ul>
                    </li>
                    <li>
                        <p><span>5.7</span> Linking to our site</p>
                        <ul class="letters-in-sections-in-content">
                            <li>You may link to our home page, provided you do so in a way that is fair and legal and does not damage our reputation or take advantage of it.</li>
                            <li>You must not establish a link in such a way as to suggest any form of association, approval or endorsement on our part where none exists.</li>
                            <li>You must not establish a link to our site in any website that is not owned by you.</li>
                            <li>Our site must not be framed on any other site, nor may you create a link to any part of our site other than the home page.</li>
                            <li>We reserve the right to withdraw linking permission without notice.</li>
                            <li>If you wish to make any use of content on our site other than that set out above, please contact info@thetrademarkcompany.co.uk.</li>
                        </ul>
                    </li>
                    <li>
                        <p><span>5.8</span> Third party links and resources in our site</p>
                        <ul class="letters-in-sections-in-content">
                            <li>Where our site contains links to other sites and resources provided by third parties, these links are provided for your information only.</li>
                            <li>We have no control over the content of those sites or resources and accept no liability in respect of them.</li>
                        </ul>
                    </li>
                </ul>
            </div>

            <div class="section-number">6. HOW WE USE YOUR PERSONAL INFORMATION</div>

            <div class="section-content">
                <ul class="sub-sections-in-content"> 
                    <li>
                        <p><span>6.1</span> We only use your personal information in accordance with our Privacy Policy <a href="http://thetrademarkcompany.co.uk/privacy-policy">here</a>. For details, please see our Privacy Policy here. Please take the time to read these, as they include important terms which apply to you </p>
                    </li>
                </ul>
            </div>

            <div class="section-number">7. GRANT OF LICENCE</div>

            <div class="section-content">
                <ul class="sub-sections-in-content"> 
                    <li>
                        <p><span>7.1</span> You are granted a non-exclusive, not transferable licence by us to:</p>
                        <ul class="letters-in-sections-in-content">
                            <li>Print pages from this web site provided you do not remove or modify any copyright, notices, trade marks or other proprietary notices;</li>
                            <li>Bookmark any freely available page or link to it;</li>
                            <li>Use either Microsoft's Internet Explorer or Netscape's Navigator web browser or the Firefox web browser or the Safari web browser to interact with the pages of the site</li>
                        </ul>
                    </li>
                    <li>
                        <p><span>7.2</span> Purchase of the right to generate a legal document results in you being granted a non-exclusive, not transferable licence by us to:</p>
                        <ul class="letters-in-sections-in-content">
                            <li>Use all the restricted access areas of the site to generate the associated legal document for a period of time reasonable to complete the document.</li>
                            <li>View all of the resulting legal document wording</li>
                            <li>Print the legal document wording that has been generated</li>
                            <li>Print guidelines explaining aspects of use of the legal document</li>
                            <li>Save the legal document wording in electronic form.</li>
                        </ul>
                    </li>
                    <li>
                        <p><span>7.3</span> Note that 'generate' is defined as the creation of only one legal document which is then your responsibility to sign and make legal.</p>
                        <ul class="letters-in-sections-in-content">
                            <li>The above grants of licence form part of the user agreement. As such each is inextricably linked to your compliance with all other aspects of the Terms.</li>
                        </ul>
                    </li>
                </ul>
            </div>

            <div class="section-number">8. OUR RIGHT TO VARY THESE TERMS</div>

            <div class="section-content">
                <ul class="sub-sections-in-content"> 
                    <li>
                        <p><span>8.1</span> We may revise these Terms from time to time including but not limited to the following circumstances:</p>
                        <ul class="letters-in-sections-in-content">
                            <li>changes to our Documents;</li>
                            <li>changes in the cost of our Documents;</li>
                            <li>changes in how we accept payment from you; and</li>
                            <li>changes in relevant laws and regulatory requirements.</li>
                        </ul>
                    </li>
                    <li>
                        <p><span>8.2</span> Every time you order Documents from us, the Terms in force at that time will apply to the Contract between you and us.</p>
                    </li>
                    <li>
                        <p><span>8.3</span> Whenever we revise these Terms in accordance with this clause 7, we will keep you informed and give you notice of this by stating that these Terms have been amended and the relevant date at the top of this page.</p>
                    </li>
                </ul>
            </div>

            <div class="section-number">9. STORAGE SERVICE</div>

            <div class="section-content">
                <ul class="sub-sections-in-content"> 
                    <li>
                        <p><span>9.1</span> If you use our Document Storage Service, you must order the Service via the Storage page on our Site. We will provide you with a preprinted Storage Envelope which you must use to forward to us those Documents you wish to store.</p>
                    </li>
                    <li>
                        <p><span>9.2</span> You may decide to pay annually for such storage, in which case we will require direct debit details which may be entered on the Storage page of our Site, or pay in advance until such time as the Documents are required, for which we will charge a one off fee to be agreed with you upon your ordering the Service.</p>
                    </li>
                    <li>
                        <p><span>9.3</span> We will store the Documents in a secure and insured facility and provide you with a reference number and contact details.</p>
                    </li>
                    <li>
                        <p><span>9.4</span> We will provide a retrieval facility, via the Retrieval page of our Site, for retrieval at any time you or your family require. Retrieval Fees shall be calculated at the time and will be dependant on the retrieval costs of the archiving service we use form time to time.</p>
                    </li>
                    <li>
                        <p><span>9.5</span> You should enter Authorised Persons on the Retrieval Page for those people, such as your Executors, who may need to retrieve the Documents. Such persons will also need to pay a Retrieval Fee and produce athe testator's Death Certificate.</p>
                    </li>
                    <li>
                        <p><span>9.6</span> You agree to keep up all annual payments for the use of our Document Storage Service, and you authorise us to destroy your Document Envelope if you fall more than 6 months behind in your payments and your Document Envelope is still in storage.</p>
                    </li>
                </ul>
            </div>
            
            <div class="section-number">10. AMENDMENTS TO THE DOCUMENTS AND REVIEW SERVICE</div>

            <div class="section-content">
                <ul class="sub-sections-in-content"> 
                    <li><p><span>10.1</span> You may re-enter a Document from your account page and make amendments and re-print at any time.</p></li>
                    <li><p><span>10.2</span> You may instruct us to perform a review of the Document by selecting "Review" on your account page. On submitting the request, you will be taken to a payment page and on submitting this, you have formally instructed us to carry out this Service for you. The Service will entail a qualified Will Writer reviewing your Document and suggesting amendments if appropriate. You may re-generate the Document upon receiving the results of the Review.</p></li>
                    <li><p><span>10.3</span> Any amendments made by you are at your own risk and on the same basis as the drafting of the Document under these Terms. Similarly, your failure to make amendments suggested by the Review will be at your own risk.</p></li>
                </ul>
            </div>

            <div class="section-number">11. CANCELLATION</div>

            <div class="section-content">
                <ul class="sub-sections-in-content"> 
                    <li><p><span>11.1</span> Upon entering into the Contract by submitting your Order Form for a Document, you enter into a Contract governed by these Terms and regulated by the Consumer Contracts (Information, Cancellation and Additional Charges) Regulations 2013 ("<b>Regulations</b>"). Under Regulation 29(1) you have a right to cancel the contract provided that you notify us within 14 days of the day following the date of submitting your Order Form.</p></li>
                    <li><p><span>11.2</span> Upon requesting Storage of your Document(s) , you may cancel the request within 14 days and will receive a full refund. Should you cancel your Storage Request outside of this time limit, you will receive a refund less any reasonable administrative costs involved in performing the request and, if relevant, you may also be liable for the Retrieval fees as set out in clause 9 above.</p></li>
                    <li><p><span>11.3</span> However, in accordance with the Regulations, you go on to submit an order for a Document you are giving your express consent to Delivery of the Document and will thereby lose your right to cancel the Document under Regulations.</p></li>
                </ul>
            </div>
            
            <div class="section-number">12. GENERAL</div>

            <div class="section-content">
                <ul class="sub-sections-in-content"> 
                    <li>
                        
                        <p><span>12.1</span> Each Party undertakes that it shall not at any time after the date of this agreement use, divulge or communicate to any person (except to its professional representatives or advisers or as may be required by law or any legal or regulatory authority) any confidential information concerning the terms of this agreement, the business or affairs of the other party which may have (or may in future) come to its knowledge, and each of the parties shall use its reasonable endeavours to prevent the publication or disclosure of any confidential information concerning such matters.</p>
                    </li>
                    <li>
                        
                        <p><span>12.2</span> Any notice given under this agreement shall be in writing and shall be delivered by hand, transmitted by email, or sent by pre-paid first class post or recorded delivery post to the address of the party as referred to in this clause. A notice sent by first class post shall be deemed served two days after posting, and on delivery if recorded. A notice sent by email to the relevant party shall be deemed to have been received at the time of transmission.</p>
                    </li>
                    <li>
                        
                        <p><span>12.3</span> If any provision of this agreement (or part of any provision) is found by any court or other body of competent jurisdiction to be invalid, unenforceable or illegal, the other provisions shall remain in force.</p>
                    </li>
                    <li>
                        
                        <p><span>12.4</span>If any invalid, unenforceable or illegal provision would be valid, enforceable and legal if some part of it were deleted, the provision shall apply with whatever modification is necessary to give effect to the commercial intention of the parties.</p>
                    </li>
                    <li>
                        
                        <p><span>12.5</span> We reserve the right to vary these Terms from time to time. Such variations will take effect immediately upon the posting of the varied Terms upon the website.</p>
                    </li>
                    <li>
                        
                        <p><span>12.6</span> No failure or delay by a party to exercise any right or remedy provided under this agreement or by law shall constitute a waiver of that or any other right or remedy, nor shall it preclude or restrict the further exercise of that or any other right or remedy. No single or partial exercise of such right or remedy shall preclude or restrict the further exercise of that or any other right or remedy.</p>
                    </li>
                    <li>
                        
                        <p><span>12.7</span> Unless specifically provided otherwise, rights and remedies arising under this agreement are cumulative and do not exclude rights and remedies provided by law.</p>
                    </li>
                    <li>
                        
                        <p><span>12.8</span> No person may assign, or grant any encumbrance over, or deal in any way with, any of his rights under this agreement or any document referred to in it or purport to do any of the same in each case without the prior written consent of all the parties for the time being (such consent not to be unreasonably conditioned, withheld or delayed).</p>
                    </li>
                    <li>
                        
                        <p><span>12.9</span> Each person that has rights under this agreement is acting on his own behalf.</p>
                    </li>
                    <li>
                        
                        <p><span>12.10</span> This agreement constitutes the whole agreement between the parties and supersedes any previous arrangement, understanding or agreement between them relating to the subject matter they cover.</p>
                    </li>
                    <li>
                        
                        <p><span>12.11</span> Each party acknowledges that, in entering into this agreement, he does not rely on, and shall have no remedy in respect of, any statement, representation, assurance or warranty of any person other than as expressly set out in this agreement or those documents.</p>
                    </li>
                    <li>
                        
                        <p><span>12.12</span> Nothing in this clause operates to limit or exclude any liability for fraud.</p>
                    </li>
                    <li>
                        
                        <p><span>12.13</span> A person who is not a party to this agreement shall not have any rights under the Contracts (Rights of Third Parties) Act 1999 to enforce or to enjoy the benefit of any term of this agreement.</p>
                    </li>
                    <li>
                        
                        <p><span>12.14</span> This agreement and any dispute or claim arising out of or in connection with it or its subject matter or formation (including non-contractual disputes or claims) shall be governed by and construed in accordance with the law of England.</p>
                    </li>
                    <li>
                        
                        <p><span>12.15</span> The parties irrevocably agree to submit to the exclusive jurisdiction of the courts of England over any claim or matter arising under or in connection with this agreement or its subject matter or formation (including non-contractual disputes or claims).</p>
                    </li>
                </ul>
            </div>
        </div>




                </div><!-- .line -->
                <a href="index.html"><button class="btn btn-success pull-right">Back</button></a>
            </div>
        </div>
        <!-- /.container -->
    </div>
@endsection