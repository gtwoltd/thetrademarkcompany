@section('title')
{{"The Trademark Company"}}
@stop


@section('description')
{{ ""}}
@stop

@extends('index')

@section('content')



    <!-- Header -->
    <header class="intro-header">
        <div class="container">

            <div class="row">
                <div class="col-lg-6">
                    <div class="cp-bg-main-opa cp-padding-card" style="margin-top:100px">
                    <h2 class="cp-strong cp-green">The UK's Number one Trademark Protection Service</h2><br>
                    <center>
                    <ul class="lead" style="list-style-type: none;text-align: left;">
                        <li><i class="fa fa-check-circle"></i> Thinking of protecting your business and brand?</li>
                        <li><i class="fa fa-check-circle"></i> Want lifelong protection?</li>
                        <li><i class="fa fa-check-circle"></i> Want to ensure the integrity of your brand?</li>
                    </ul>
                    </center>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="cp-bg-white-opa cp-padding-card" style="margin-top:101px">
                        <img src="{{ asset('assets/img/logo3.png') }}" class="cp-logo-down" style="margin-top:0px" alt="trademark company logo" title="The Trademark Company Logo">
                        <h2 class="cp-main">Call us to find out more</h2><br>
                        <h2 class="cp-main cp-strong" style="margin-top:-14px">0345 052 2759</h2>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.container -->

    </header>
    <!-- /.intro-header -->


    <div class="content-section-c">

        <div class="container">
            <div class="row">
                <div class="line col-md-12 col-sm-12">
                    <center>
                       <h2 id="search" class="section-heading text-center cp-main cp-strong">Free Trademark Tool</h2>
                       <p class="cp-main text-center section-heading cp-margin-top-contact">Use our free trademark tool to discover if your trademark is likely to be accepted<p>
                       <form>
                            <!-- large screen -->
                            <div class="col-lg-12 hidden-xs hidden-md" style="width:40%;margin-left:340px">
                                <div class="input-group">
                                  <input type="text" class="form-control cp-margin-top-contact" placeholder="Enter your business name here ...">
                                  <span class="input-group-btn">
                                    <button class="cp-btn btn-success cp-margin-top-contact" type="button">Go!</button>
                                  </span>
                                </div><!-- /input-group -->
                            </div>
                            <!-- smaller screen -->
                            <div class="col-lg-12 visible-xs visible-md">
                                <div class="input-group">
                                  <input type="text" class="form-control cp-margin-top-contact" placeholder="Enter your business name here ...">
                                  <span class="input-group-btn">
                                    <button class="cp-btn btn-success cp-margin-top-contact" type="button">Go!</button>
                                  </span>
                                </div><!-- /input-group -->
                            </div>
                            <br><br>
                            <input class="styled" id="radio1" type="radio" name="database" value="uk" checked>
                            <label for="radio1"> UK</label> 
                            <input class="styled" id="radio2" type="radio" name="database" value="ctm">
                            <label for="radio2"> EU</label>
                            
                       </form>
                   </center>
                </div><!-- .line -->
            </div>
        </div>
        <!-- /.container -->
    </div>
    <!-- /.content-section-c -->

    <!-- Page Content -->

    <a  name="about"></a>
    <div class="content-section-a">

        <div class="container">
            <div class="row">
                <div class="line col-md-12 col-sm-12" itemscope itemtype="https://schema.org/Thing">
                    <div class="cols col-md-6 col-xs-12">
                        <div class="imgs col-md-3">
                            <img src="{{ asset('assets/img/compliance.png') }}" class="cp-img" alt="trademark icon" title="trademark company complete compliance">
                        </div><!-- .imgs -->
                        <div class="txt col-md-9 col-xs-12">
                            <h3 class="cp-green cp-strong"><span itemprop="name">Complete compliance with the Trademarks Act</span></h3>
                            <p class="cp-main"><span itemprop="description">When registering your trademark, we ensure that we make stringent checks to make sure that your trademark will be accepted and is compliant with the Trademark Act</span></p>
                        </div><!-- .txt -->
                    </div><!-- .cols -->

                    <div class="cols col-md-6 col-xs-12">
                        <div class="imgs col-md-3">
                            <img src="{{ asset('assets/img/team.png') }}" class="cp-img" alt="trademark icon" title="trademark company professional in-house team">
                        </div><!-- .imgs -->
                        <div class="txt col-md-9 col-xs-12">
                            <h3 class="cp-green cp-strong"><span itemprop="name">Dedicated professional in-house team</span></h3>
                            <p class="cp-main"><span itemprop="description">Our dedicated, professional in-house team have years of experience with creating trademarks, they know what will and won't be accepted, and can help guide you through the entire process</span></p>
                        </div><!-- .txt -->
                    </div><!-- .cols -->
                    <div class="cols col-md-6 col-xs-12">
                        <div class="imgs col-md-3">
                            <img src="{{ asset('assets/img/protect.png') }}" class="cp-img" alt="trademark icon" title="trademark company full protection">
                        </div><!-- .imgs -->
                        <div class="txt col-md-9 col-xs-12">
                            <h3 class="cp-green cp-strong"><span itemprop="name">Full company 
                            protection</span></h3>
                            <p class="cp-main"><span itemprop="description">We'll help you to completely protect your business and brand with a trademark, ensuring that no one can steal the integrity of your business</span></p>
                        </div><!-- .txt -->
                    </div><!-- .cols -->
                    <div class="cols col-md-6 col-xs-12">
                        <div class="imgs col-md-3">
                            <img src="{{ asset('assets/img/work.png') }}" class="cp-img" alt="trademark icon" title="trademark company doing the hard work for you">
                        </div><!-- .imgs -->
                        <div class="txt col-md-9 col-xs-12">
                            <h3 class="cp-green cp-strong"><span itemprop="name">Doing the hard work for you</span></h3>
                            <p class="cp-main"><span itemprop="description">We'll help you to complete protect your business and brand with a trademark, ensuring that no one can steal integrity of your business</span></p>
                        </div><!-- .txt -->
                    </div><!-- .cols -->
                </div><!-- .line -->
            </div>
        </div>
        <!-- /.container -->
    </div>
    <!-- /.content-section-a -->


    <a name="crm"></a>
    <div class="content-section-c-c">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-md-12 cp-padding-c" itemscope itemtype="https://schema.org/CreativeWork">
                    <div class="clearfix"></div>
                    <h2 class="section-heading text-center cp-main cp-strong">Why do you need a <span itemprop="creator">trademark?</span></h2>
                    <div itemprop="about">
                    <p class="lead text-center cp-main">Without a trademark, you don't have many rights. Whilst you're busy building your brand, someone else could come and copy your brand entirely, potentially damaging your business irreparably, and the worst part, there's nothing you can do.</p>
                    <p class="lead text-center cp-main">That's why you need a trademark. A trademark will allow you to sue anyone infringing your trademark, as well as allowing you to take action against counterfeits. A trademark helps you to build brand recognition and loyalty, as well as giving you protection for 10 years before you need to renew it.</p>
                    </div>
                    <div id="video">
                        <div id="mac">
                            <div id="logo" class="col-lg-12 col-md-12 col-sm-12">

                            <!-- Please use this iframe if you were to put some video link, this is supported for large and smaller screens just remove the comment -->
                            <!-- <iframe class="hidden-xs" width="680" height="325" src="https://www.youtube.com/embed/uSjMPvNONpU?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                            </div>
                            <iframe class="visible-xs" style="margin-left:14px;margin-top:105px" width="355" height="175" src="https://www.youtube.com/embed/uSjMPvNONpU?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container -->

    </div>
    <!-- /.content-section-c-c -->


    <a name="classes"></a>
    <div class="content-section-a">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12">
                    <center>
                        <h2 class="section-heading cp-main cp-strong text-center">Why the Trademark Company?</h2>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                            <div class="cp-card-brd">
                                <p class="lead cp-green cp-padding-20"> <strong>UK's number 1<br>Trademark Service</strong></p>
                                <i class="fa fa-check fa-5x cp-main"></i>
                                <p class="lead cp-main cp-padding-text"><br>We are proud to say that we are the UK's number one trademark service - we provide you with the best and most professional service</p><br>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                            <div class="cp-card-brd">
                                <p class="lead cp-green cp-padding-20"><strong>Qualified Trademark<br>Attorneys</strong></p>
                                <i class="fa fa-mortar-board fa-5x cp-main"></i>
                                <p class="lead cp-main cp-padding-text"><br>Our qualified trademark attorneys will ensure that your trademark is completely compliant with The Trademark Act</p><br>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                            <div class="cp-card-brd">
                                <p class="lead cp-green cp-padding-20"><strong>Free Trademark<br>Search</strong></p>
                                <i class="fa fa-search fa-5x cp-main"></i>
                                <p class="lead cp-main cp-padding-text"><br>You can check before you even purchase a package from us to ensure that your trademark is likely to be accepted</p><br>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                            <div class="cp-card-brd">
                                <p class="lead cp-green cp-padding-20"><strong>97% Success<br>Rate</strong></p>
                                <i class="fa fa-trophy fa-5x cp-main"></i>
                                <p class="lead cp-main cp-padding-text"><br>We are proud to say that we have a 97% success rate at getting your trademarks successfully registered</p><br>
                            </div>
                        </div>
                    </center>
                </div>
            </div>
        </div>
        <!-- /.container -->
    </div>
    <!-- /.content-section-a -->

    <a name="pricing"></a>
    <div class="content-section-c">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12" itemscope itemtype="http://schema.org/Product">
                    <center>
                        <h2 class="section-heading cp-main cp-strong text-center"><span itemprop="name">Trademark Packages</span></h2>
                        <table class="table table-bordered text-center strong cp-bg-white" id="comparetable">
                        <tr class="tb-yellow">
                            <th class="text-center trans cp-pack" rowspan="2"></th>
                            <th class="text-center cp-bg-main cp-white cp-strong" rowspan="2">UK Standard</th>
                            <th class="text-center cp-bg-main cp-white cp-strong" rowspan="2">EU Standard</th>
                            <th class="text-center cp-bg-main cp-white cp-strong" rowspan="2">UK Full Clearance</th>
                            <th class="text-center cp-bg-main cp-white cp-strong" rowspan="2">UK Full Clearance<br>and Analysis</th>
                        </tr>
                        <tr>
                        </tr>
                        <tr itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                            <th class="text-center trans cp-pack-mid" rowspan="2"></th>
                            <th class="text-center cp-main" rowspan="2"><h3><span itemprop="price">£199</span></h3></th>
                            <th class="text-center cp-main" rowspan="2"><h3><span itemprop="price">£199</span></h3></th>
                            <th class="text-center cp-main" rowspan="2"><h3><span itemprop="price">£699</span></h3></th>
                            <th class="text-center cp-main" rowspan="2"><h3><span itemprop="price">£1,399</span></h3></th>
                        </tr>
                        <tr>
                        </tr>
                        <tr style="background-color: #fff">
                            <td class="cp-font-custom cp-main">Full Search</td>
                            <td class="cp-font-custom cp-main"><center><i class="fa fa-check fa-2x cp-green"></i></center></td>
                            <td class="cp-font-custom cp-main"><center><i class="fa fa-check fa-2x cp-green"></i></center></td>
                            <td class="cp-font-custom cp-main"><center><i class="fa fa-check fa-2x cp-green"></i></center></td>
                            <td class="cp-font-custom cp-main"><center><i class="fa fa-check fa-2x cp-green"></i></center></td>
                        </tr>
                        <tr>
                            <td class="cp-font-custom cp-main">Filing Strategy and Guidance</td>
                            <td class="cp-font-custom cp-main"><center><i class="fa fa-check fa-2x cp-green"></i></center></td>
                            <td class="cp-font-custom cp-main"><center><i class="fa fa-check fa-2x cp-green"></i></center></td>
                            <td class="cp-font-custom cp-main"><center><i class="fa fa-check fa-2x cp-green"></i></center></td>
                            <td class="cp-font-custom cp-main"><center><i class="fa fa-check fa-2x cp-green"></i></center></td>
                        </tr>
                        <tr style="background-color: #fff">
                            <td class="cp-font-custom cp-main">Specification of Goods and Services Preparation</td>
                            <td class="cp-font-custom cp-main"><center><i class="fa fa-check fa-2x cp-green"></i></center></td>
                            <td class="cp-font-custom cp-main"><center><i class="fa fa-check fa-2x cp-green"></i></center></td>
                            <td class="cp-font-custom cp-main"><center><i class="fa fa-check fa-2x cp-green"></i></center></td>
                            <td class="cp-font-custom cp-main"><center><i class="fa fa-check fa-2x cp-green"></i></center></td>
                        </tr>
                        <tr>
                            <td class="cp-font-custom cp-main">Application Preparation</td>
                            <td class="cp-font-custom cp-main"><center><i class="fa fa-check fa-2x cp-green"></i></center></td>
                            <td class="cp-font-custom cp-main"><center><i class="fa fa-check fa-2x cp-green"></i></center></td>
                            <td class="cp-font-custom cp-main"><center><i class="fa fa-check fa-2x cp-green"></i></center></td>
                            <td class="cp-font-custom cp-main"><center><i class="fa fa-check fa-2x cp-green"></i></center></td>
                        </tr>
                        <tr style="background-color: #fff">
                            <td class="cp-font-custom cp-main">Filing of Application</td>
                            <td class="cp-font-custom cp-main"><center><i class="fa fa-check fa-2x cp-green"></i></center></td>
                            <td class="cp-font-custom cp-main"><center><i class="fa fa-check fa-2x cp-green"></i></center></td>
                            <td class="cp-font-custom cp-main"><center><i class="fa fa-check fa-2x cp-green"></i></center></td>
                            <td class="cp-font-custom cp-main"><center><i class="fa fa-check fa-2x cp-green"></i></center></td>
                        </tr>
                        
                        <tr>
                            <td class="cp-font-custom cp-main">100% Satisfaction Guarantee</td>
                            <td class="cp-font-custom cp-main"><center><i class="fa fa-check fa-2x cp-green"></i></center></td>
                            <td class="cp-font-custom cp-main"><center><i class="fa fa-check fa-2x cp-green"></i></center></td>
                            <td class="cp-font-custom cp-main"><center><i class="fa fa-check fa-2x cp-green"></i></center></td>
                            <td class="cp-font-custom cp-main"><center><i class="fa fa-check fa-2x cp-green"></i></center></td>
                        </tr>
                        <tr style="background-color: #fff">
                            <td class="cp-font-custom cp-main">Full Due Diligence Search</td>
                            <td class="cp-font-custom cp-main"></td>
                            <td class="cp-font-custom cp-main"></td>
                            <td class="cp-font-custom cp-main"><center><i class="fa fa-check fa-2x cp-green"></i></center></td>
                            <td class="cp-font-custom cp-main"><center><i class="fa fa-check fa-2x cp-green"></i></center></td>
                        </tr>
                        <tr>
                            <td class="cp-font-custom cp-main">Full Due Diligence Search with Analysis report</td>
                            <td class="cp-font-custom cp-main"></td>
                            <td class="cp-font-custom cp-main"></td>
                            <td class="cp-font-custom cp-main"></td>
                            <td class="cp-font-custom cp-main"><center><i class="fa fa-check fa-2x cp-green"></i></center></td>
                        </tr>
                        <tr style="background-color: #fff">
                            <td class="cp-strong cp-green" colspan="5">Bonus Materials</td> 
                        </tr>
                        <tr>
                            <td class="cp-font-custom cp-main">Why Value your business guide</td>
                            <td class="cp-font-custom cp-main"><center><i class="fa fa-check fa-2x cp-green"></i></center></td>
                            <td class="cp-font-custom cp-main"><center><i class="fa fa-check fa-2x cp-green"></i></center></td>
                            <td class="cp-font-custom cp-main"><center><i class="fa fa-check fa-2x cp-green"></i></center></td>
                            <td class="cp-font-custom cp-main"><center><i class="fa fa-check fa-2x cp-green"></i></center></td>
                        </tr>
                        <tr style="background-color: #fff">
                            <td class="cp-font-custom cp-main">How to make more profit – Mini Guide for Small Businesses</td>
                            <td class="cp-font-custom cp-main"><center><i class="fa fa-check fa-2x cp-green"></i></center></td>
                            <td class="cp-font-custom cp-main"><center><i class="fa fa-check fa-2x cp-green"></i></center></td>
                            <td class="cp-font-custom cp-main"><center><i class="fa fa-check fa-2x cp-green"></i></center></td>
                            <td class="cp-font-custom cp-main"><center><i class="fa fa-check fa-2x cp-green"></i></center></td>
                        </tr>
                        <tr>
                            <td class="cp-font-custom cp-main">The Secrets of how big companies remunerate themselves in a tax efficient manner (the things your accountant doesn’t know)</td>
                            <td class="cp-font-custom cp-main"></td>
                            <td class="cp-font-custom cp-main"></td>
                            <td class="cp-font-custom cp-main"><center><i class="fa fa-check fa-2x cp-green"></i></center></td>
                            <td class="cp-font-custom cp-main"><center><i class="fa fa-check fa-2x cp-green"></i></center></td>
                        </tr>
                        <tr style="background-color: #fff">
                            <td class="cp-font-custom cp-main">A Guide to The importance of having a Company Will</td>
                            <td class="cp-font-custom cp-main"></td>
                            <td class="cp-font-custom cp-main"></td>
                            <td class="cp-font-custom cp-main"><center><i class="fa fa-check fa-2x cp-green"></i></center></td>
                            <td class="cp-font-custom cp-main"><center><i class="fa fa-check fa-2x cp-green"></i></center></td>
                        </tr>
                        <tr>
                            <td class="cp-strong cp-green" colspan="5">Additional Services</td> 
                        </tr>
                        <tr  style="background-color: #fff">
                            <td class="cp-font-custom cp-main">Remuneration Report – find out how to pay less tax, the things your accountant doesn’t know</td>
                            <td class="cp-font-custom cp-main"><center>10% discount</center></td>
                            <td class="cp-font-custom cp-main"><center>10% discount</center></td>
                            <td class="cp-font-custom cp-main"><center>15% discount</center></td>
                            <td class="cp-font-custom cp-main"><center>20% discount</center></td>
                        </tr>
                        <tr>
                            <td class="cp-font-custom cp-main">Business Valuation Services</td>
                            <td class="cp-font-custom cp-main"><center>10% discount</center></td>
                            <td class="cp-font-custom cp-main"><center>10% discount</center></td>
                            <td class="cp-font-custom cp-main"><center>15% discount</center></td>
                            <td class="cp-font-custom cp-main"><center>20% discount</center></td>
                        </tr>
                        <tr  style="background-color: #fff">
                            <td class="cp-font-custom cp-main">Business Planning Services</td>
                            <td class="cp-font-custom cp-main"><center>10% discount</center></td>
                            <td class="cp-font-custom cp-main"><center>10% discount</center></td>
                            <td class="cp-font-custom cp-main"><center>15% discount</center></td>
                            <td class="cp-font-custom cp-main"><center>20% discount</center></td>
                        </tr>
                        <tr>
                            <th class="trans cp-pack-bot"  ></th>
                            <td class="trans cp-pack-bot"><a href="https://qk243.infusionsoft.com/app/orderForms/Trademark-Company-UK-Standard" class="form-control cp-btn-cont btn-success cp-white cp-strong cp-height">Buy Now</a></td>
                            <td class="trans cp-pack-bot"><a href="https://qk243.infusionsoft.com/app/orderForms/Trademark-Company-EU-Standard" class="form-control cp-btn-cont btn-success cp-white cp-strong cp-height">Buy Now</a></td>
                            <td class="trans cp-pack-bot"><a href="https://qk243.infusionsoft.com/app/orderForms/Trademark-Company-UK-Full-Clearance" class="form-control cp-btn-cont btn-success cp-white cp-strong cp-height">Buy Now</a></td>
                            <td class="trans cp-pack-bot"><a href="https://qk243.infusionsoft.com/app/orderForms/Trademark-Company-UK-Full-Clearance-and-Analysis" class="form-control cp-btn-cont btn-success cp-white cp-strong cp-height">Buy Now</a></td>
                        </tr>
                        </table>
                    </center>
                </div>
            </div>
        </div>
        <!-- /.container -->
    </div>
    <!-- /.content-section-c-c -->

    <a  name="testimonials"></a>
    <div class="content-section-d">

        <div class="container" id="testimonial">

            <div class="row">
                <div class="col-lg-12 col-sm-12">
                    <center>
                        <h2 class="text-center cp-main cp-strong">Testimonials</h2>
                        <p class="section-heading"></p>
                        <div id="testimonial">
                        <div class="container hidden-xs">
                            
                            <div class="box-container">

                                <div class="testi-box col-lg-5 col-md-10 col-sm-10 cp-mxht">
                                    <div class="img"><img src="{{ asset('assets/img/testimonial-1.png') }}" alt="trademark testimonial" title="trademark company seth cohen testimonial"></div><!-- .img -->
                                    <p class="cp-main">"The Trademark Company have been so helpful in getting my trademark registered. Their free search tool was fantastic and enabled me to find out if my trademark was likely to be registered, before I started the process. The while thing was so easy and a fantastic help, I'd recommend them to anyone"<br>
                                    <span class="cp-green cp-strong">Seth Cohen<br>Bedforshire</span></p>
                                    <br>
                                </div>

                                <div class="testi-box col-lg-5 col-md-10 col-sm-10 cp-mxht">
                                    <div class="img"><img src="{{ asset('assets/img/testimonial-2.png') }}" alt="trademark testimonial" title="trademark company lori ferguson testimonial"></div><!-- .img -->
                                    <p class="cp-main">“The Trademark Company specialised not just in UK applications, but EU applications as well, which is great as i really wanted to ensure i was completely protected. They were helpful from start to finish and the process couldn't have been easier”<br>
                                    <span class="cp-green cp-strong">Lori Ferguson<br>South Yorkshire</span></p>
                                    <br>
                                </div>

                            </div>

                                <div class="testi-box col-lg-5 col-md-10 col-sm-10 hidden-xs hidden-sm hidden-md cp-mxht2" style="margin-left:360px">
                                    <div class="img"><img src="{{ asset('assets/img/testimonial-3.png') }}" alt="trademark testimonial" title="trademark company joanne scott testimonial"></div><!-- .img -->
                                    <p class="cp-main">“I knew I needed a trademark but the process of registering one was so overwhelming. I decided to get on contact with the Trademark Company. Their free trademark search was so great as they were able to tell me my trademark was likely to be registered, and allowed me to make sure i wanted to continue with the application. They took care of everything, I couldn't fault them!”<br>
                                    <span class="cp-green cp-strong">Joanne Scott<br>Hampshire</span></p>
                                    <br>
                                </div>

                                <div class="testi-box col-lg-5 col-md-10 col-sm-10 visible-xs visible-sm visible-md cp-mxht2">
                                    <div class="img"><img src="{{ asset('assets/img/testimonial-3.png') }}" alt="trademark testimonial" title="trademark company joanne scott testimonial"></div><!-- .img -->
                                    <p class="cp-main">“I knew I needed a trademark but the process of registering one was so overwhelming. I decided to get on contact with the Trademark Company. Their free trademark search was so great as they were able to tell me my trademark was likely to be registered, and allowed me to make sure i wanted to continue with the application. They took care of everything, I couldn't fault them!”<br>
                                    <span class="cp-green cp-strong">Joanne Scott<br>Hampshire</span></p>
                                    <br>
                                </div>
                        </div><!-- .container hidden xs-->

                        <div class="container visible-xs" id="testimonial-xs">
                            
                            <div class="box-container-xs">

                                <div class="testi-box-xs col-lg-5 col-md-10 col-sm-10 cp-mxht">
                                    <div class="img"><img src="{{ asset('assets/img/testimonial-1.png') }}" alt="trademark testimonial" title="trademark company seth cohen testimonial"></div><!-- .img -->
                                    <p class="cp-main">"The Trademark Company have been so helpful in getting my trademark registered. Their free search tool was fantastic and enabled me to find out if my trademark was likely to be registered, before I started the process. The while thing was so easy and a fantastic help, I'd recommend them to anyone"<br>
                                    <span class="cp-green cp-strong">Seth Cohen<br>Bedforshire</span></p>
                                    <br>
                                </div>

                                <div class="testi-box-xs col-lg-5 col-md-10 col-sm-10 cp-mxht">
                                    <div class="img"><img src="{{ asset('assets/img/testimonial-2.png') }}" alt="trademark testimonial" title="trademark company lori ferguson testimonial"></div><!-- .img -->
                                    <p class="cp-main">“The Trademark Company specialised not just in UK applications, but EU applications as well, which is great as i really wanted to ensure i was completely protected. They were helpful from start to finish and the process couldn't have been easier”<br>
                                    <span class="cp-green cp-strong">Lori Ferguson<br>South Yorkshire</span></p>
                                    <br>
                                </div>

                            </div>

                                <div class="testi-box-xs col-lg-5 col-md-10 col-sm-10 hidden-xs hidden-sm hidden-md cp-mxht2" style="margin-left:360px">
                                    <div class="img"><img src="{{ asset('assets/img/testimonial-3.png') }}" alt="trademark testimonial" title="trademark company joanne scott testimonial"></div><!-- .img -->
                                    <p class="cp-main">“I knew I needed a trademark but the process of registering one was so overwhelming. I decided to get on contact with the Trademark Company. Their free trademark search was so great as they were able to tell me my trademark was likely to be registered, and allowed me to make sure i wanted to continue with the application. They took care of everything, I couldn't fault them!”<br>
                                    <span class="cp-green cp-strong">Joanne Scott<br>Hampshire</span></p>
                                    <br>
                                </div>

                                <div class="testi-box-xs col-lg-5 col-md-10 col-sm-10 visible-xs visible-sm visible-md cp-mxht2">
                                    <div class="img"><img src="{{ asset('assets/img/testimonial-3.png') }}" alt="trademark testimonial" title="trademark company joanne scott testimonial"></div><!-- .img -->
                                    <p class="cp-main">“I knew I needed a trademark but the process of registering one was so overwhelming. I decided to get on contact with the Trademark Company. Their free trademark search was so great as they were able to tell me my trademark was likely to be registered, and allowed me to make sure i wanted to continue with the application. They took care of everything, I couldn't fault them!”<br>
                                    <span class="cp-green cp-strong">Joanne Scott<br>Hampshire</span></p>
                                    <br>
                                </div>
                        </div><!-- .container visible xs-->

                    </div><!-- #testimonial -->

                    </center>
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.content-section-d -->

    <a name="contact"></a>
    <div class="content-section-c">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12">
                    <center>
                        <h2 class="section-heading cp-main text-center cp-strong">Contact Us</h2>
                        <h3 class="section-heading cp-main text-center cp-margin-top-contact">Email: info@thetrademarkcompany.co.uk</h3>
                        <div class="cp-bg-grey cp-padding-card hidden-xs" style="width:50%">
                        


<!--                         <form action="send/contactus" method="post">
                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                            <input class="form-control" type="text" placeholder="Name" name="name" required><br>
                            <input class="form-control" type="text" placeholder="Contact Number" name="contactphone" required><br>
                            <input class="form-control" type="email" placeholder="Your Email Address" name="email" required><br>
                            <input class="form-control" type="text" placeholder="Where did you hear about us from?" name="hearphone" required><br>
                            <textarea class="form-control" name="message" placeholder="Message"></textarea><br>
                            <input class="form-control cp-btn-cont btn-success lead cp-btn-width cp-white cp-height" type="submit" value="Send" name="submit">
                        </form> -->



<form accept-charset="UTF-8" action="https://qk243.infusionsoft.com/app/form/process/4b0998b98566b4ede3332a6b2b2807ce" class="infusion-form" method="POST">
    <input name="inf_form_xid" type="hidden" value="4b0998b98566b4ede3332a6b2b2807ce" />
    <input name="inf_form_name" type="hidden" value="Contact Form - Trademark" />
    <input name="infusionsoft_version" type="hidden" value="1.42.0.44" />

        <input required placeholder="First Name" class="infusion-field-input-container form-control" id="inf_field_FirstName" name="inf_field_FirstName" type="text" />
        <br>
        <input required placeholder="Last Name" class="infusion-field-input-container form-control" id="inf_field_LastName" name="inf_field_LastName" type="text" />
<br>
        <input required placeholder="Email" class="infusion-field-input-container form-control" id="inf_field_Email" name="inf_field_Email" type="text" />
<br>
        <input required placeholder="Phone" class="infusion-field-input-container form-control" id="inf_field_Phone1" name="inf_field_Phone1" type="text" />
<br>
        <input required placeholder="Where did you hear us? " class="infusion-field-input-container form-control" id="inf_custom_Referral" name="inf_custom_Referral" type="text" />
<br>
        <input required placeholder="Enquiry" class="infusion-field-input-container form-control" id="inf_custom_Enquiry" name="inf_custom_Enquiry" type="text" />
<br>
        <input type="submit" value="Submit" class="form-control cp-btn-cont btn-success lead cp-btn-width cp-white cp-height" />

</form>

<script type="text/javascript" src="https://qk243.infusionsoft.com/app/webTracking/getTrackingCode?trackingId=e6ebba2e3123cbcbaa48a9dd4a303dec"></script>








                        </ul>
                        </div>

                        <div class="cp-bg-grey cp-padding-card visible-xs">



<form accept-charset="UTF-8" action="https://qk243.infusionsoft.com/app/form/process/4b0998b98566b4ede3332a6b2b2807ce" class="infusion-form" method="POST">
    <input name="inf_form_xid" type="hidden" value="4b0998b98566b4ede3332a6b2b2807ce" />
    <input name="inf_form_name" type="hidden" value="Contact Form - Trademark" />
    <input name="infusionsoft_version" type="hidden" value="1.42.0.44" />

        <input required placeholder="First Name" class="infusion-field-input-container form-control" id="inf_field_FirstName" name="inf_field_FirstName" type="text" />
        <br>
        <input required placeholder="Last Name" class="infusion-field-input-container form-control" id="inf_field_LastName" name="inf_field_LastName" type="text" />
<br>
        <input required placeholder="Email" class="infusion-field-input-container form-control" id="inf_field_Email" name="inf_field_Email" type="text" />
<br>
        <input required placeholder="Phone" class="infusion-field-input-container form-control" id="inf_field_Phone1" name="inf_field_Phone1" type="text" />
<br>
        <input required placeholder="Where did you hear us? " class="infusion-field-input-container form-control" id="inf_custom_Referral" name="inf_custom_Referral" type="text" />
<br>
        <input required placeholder="Enquiry" class="infusion-field-input-container form-control" id="inf_custom_Enquiry" name="inf_custom_Enquiry" type="text" />
<br>
        <input type="submit" value="Submit" class="form-control cp-btn-cont btn-success lead cp-btn-width cp-white cp-height" />

</form>

<script type="text/javascript" src="https://qk243.infusionsoft.com/app/webTracking/getTrackingCode?trackingId=e6ebba2e3123cbcbaa48a9dd4a303dec"></script>





                        </ul>
                        </center>
                        </div>
                    </center>
                </div>
            </div>
        </div>
        <!-- /.container -->
    </div>
    <!-- /.content-section-c -->

@endsection