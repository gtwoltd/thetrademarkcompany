x<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name=" description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    

    <meta name="description" content="@yield('description')">
    <title>@yield('title')</title>

    <!-- <title>The Professional Business Valuers</title> -->


    <!-- Favicon Icon -->
    <link rel="icon" href="{{ asset('assets/img/favicon.png') }}" type="image/x-icon">


    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ asset('assets/css/landing-page.css') }}" rel="stylesheet">


    <!-- Custom Fonts -->
    <link href="{{ asset('assets/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->



</head>
<body>

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-MW6C2C"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MW6C2C');</script>
<!-- End Google Tag Manager -->

@yield('tagmanager')

    <nav class="navbar navbar-default cp-bg-main navbar-fixed-top topnav" role="navigation">
        <div class="container topnav">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header cp-padding-nav cp-font-lg ">
                <a class="navbar-brand topnav cp-strong hidden-xs" href="{{ url('/') }}"><img src="{{ asset('assets/img/logo-sm.png') }}" alt="trademark company logo" title="The Trademark Company Logo"></a>
                <a href="index.html" class="cp-white visible-xs" style="padding-left:20px;margin-top:12px;float:left">Trademark <span class="cp-green">Company</span></a>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse cp-padding-nav" id="bs-example-navbar-collapse-1" itemscope itemtype="https://schema.org/Thing">
                <ul class="nav navbar-nav navbar-left cp-font-md">
                    <li>
                        <a itemprop="url" href="{{ url('/') }}#about" class="cp-green">About Us</a>
                    </li>
                    <li>
                        <a itemprop="url" href="{{ url('/') }}#step" class="cp-green">3 Step Process</a>
                    </li>
                    <li>
                        <a itemprop="url" href="{{ url('/') }}#classes" class="cp-green">Classes</a>
                    </li>
                    <li>
                        <a itemprop="url" href="{{ url('/') }}#pricing" class="cp-green">Pricing</a>
                    </li>
                    <li class="dropdown">
                         <a itemprop="url" href="#" class="dropdown-toggle cp-green" data-toggle="dropdown">Packages <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li>
                              <a itemprop="url" href="{{ url('package/uk-standard') }}">UK Standard</a>
                            </li>       
                            <li>
                              <a itemprop="url" href="{{ url('package/eu-standard') }}">EU Standard</a>
                            </li>   
                            <li>
                              <a itemprop="url" href="{{ url('package/uk-full-clearance') }}">UK Full Clearance</a>
                            </li>   
                            <li>
                              <a itemprop="url" href="{{ url('package/uk-full-clearance-and-analysis') }}">UK Full Clearance and Analysis</a>
                            </li>   
                        </ul>
                    </li>

                    <li>
                        <a itemprop="url" href="{{ url('/') }}#testimonials" class="cp-green">Testimonials</a>
                    </li>
                    <li class="dropdown">
                        <a itemprop="url" href="#" class="dropdown-toggle cp-green" data-toggle="dropdown">Helpful Information <span class="caret"></span></a>
                         <ul class="dropdown-menu">
                            <li>
                              <a itemprop="url" href="{{ url('trade-mark-applications-and-registration')}}">Trademark applications and registration</a>
                            </li>  
                            <li>
                              <a itemprop="url" href="{{ url('changes-and-renewals-of-your-registered-trade-marks')}}">Changes and renewals of your registered trade marks</a>
                            </li>  
                            <li>
                              <a itemprop="url" href="{{ url('the-trademark-companys-service-and-how-we-work')}}">The Trademark Company’s service and how we work</a>
                            </li>  
                            <li>
                              <a itemprop="url" href="{{ url('what-is-a-trademark')}}">About Trademarks</a>
                            </li>  
                            <li>
                              <a itemprop="url" href="{{ url('classification-of-goods')}}">Classification of goods</a>
                            </li>  
                        </ul>
                    </li>

                    <li>
                        <a itemprop="url" href="{{ url('blog') }}" class="cp-green">Blog</a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a itemprop="url" class="cp-white cp-font-md"><span itemscope itemtype="https://schema.org/PostalAddress"><span itemprop="telephone">0345 052 2759</span></span></a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

<?php if(!empty(Session::get('message')) ){ ?>
<div class="container" style=" position: absolute; left: 50%; transform: translateX(-50%);z-index:999">
<div class="alert alert-info ">
<div><strong>Message:</strong></div>
<p>Successfully send!</p>
</div>
</div>
<?php } ?>


@yield('content')


    <!-- Footer -->
    <footer>
        <div class="container" itemscope itemtype="https://schema.org/Thing">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="col-lg-3 col-md-6 col-sm-6 visible-xs">
                        <ul style="list-style-type: none;margin-left:-39px;float:left">
                            <li>
                                <a itemprop="url" class="cp-grey cp-deco-none">Find us on</a>
                            </li>
                            <li>
                                <a itemprop="url" href="#"><i class="fa fa-facebook-square fa-2x cp-facebook"></i></a>
                                <a itemprop="url" href="#"><i class="fa fa-twitter-square fa-2x cp-twitter"></i></a>
                            </li>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 visible-xs" style="margin-left:170px">
                        <ul class="list-inline">
                            <li>
                                <a itemprop="url" class="cp-grey cp-deco-none">Call us to find out more</a>
                            </li>
                        </ul> 
                        <div style="margin-top:-15px">
                            <h3>
                                <span class="cp-white">0000 0000 000</span>
                            </h3>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <ul class="list-inline">
                            <li>
                                <a itemprop="url" class="cp-green" href="{{ url('/') }}#about">About</a>
                            </li>
                            <li>
                                <a itemprop="url" class="cp-green" href="{{ url('/') }}#step">3 Step Process</a>
                            </li>
                            <li>
                                <a itemprop="url" class="cp-green" href="{{ url('/') }}#classes">Classes</a>
                            </li>
                            <li>
                                <a itemprop="url" class="cp-green" href="{{ url('/') }}#pricing">Pricing</a>
                            </li>
                            <li>
                                <a itemprop="url" class="cp-green hidden-xs" href="{{ url('/') }}#testimonials">Testimonials</a>
                            </li>
                        </ul>
                        <!-- <p class="cp-grey small">Rates are variable dependant on circumstances and will be discussed in full once an assessment has been made.</p> -->
                        <ul class="list-inline">
                            <li>
                                <a itemprop="url" class="cp-green" href="{{ url('terms-and-conditions') }}">Terms and Conditions</a>
                            </li>
                            <li>
                                <a itemprop="url" class="cp-green" href="{{ url('privacy-policy') }}">Privacy Policy</a>
                            </li>
                        </ul>
                        <p class="copyright cp-grey text-muted small" itemscope itemtype="https://schema.org/WPFooter"><span itemprop="copyrightYear"> &copy; 2015 </span> <a class="cp-deco-none" itemprop="creator">The Trademark Company</a>. All Rights Reserved.</p>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 hidden-xs">
                        <ul style="list-style-type: none;">
                            <li>
                                <a itemprop="url" class="cp-grey cp-deco-none">Find us on</a>
                            </li>
                            <li>
                                <a itemprop="url" href="#"><i class="fa fa-facebook-square fa-2x cp-facebook"></i></a>
                                <a itemprop="url" href="#"><i class="fa fa-twitter-square fa-2x cp-twitter"></i></a>
                            </li>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 hidden-xs">
                        <ul class="list-inline">
                            <li>
                                <a itemprop="url" class="cp-grey cp-deco-none">Call us to find out more</a>
                            </li>
                        </ul> 
                        <div style="margin-top:-15px">
                            <h3>
                                <span class="cp-white" itemscope itemtype="https://schema.org/PostalAddress"><span itemprop="telephone">0345 052 2759</span></span>
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>



    <!-- jQuery -->
    <script src="{{ asset('assets/js/jquery.js') }}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>

    <script src="{{ asset('assets/js/script.js') }}"></script>

    <script src="{{ asset('assets/js/jquery.circlechart.js') }}"></script>


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-60073042-11', 'auto');
  ga('send', 'pageview');
</script>


<script type="text/javascript">
var __lc = {};
__lc.license = 6379881;

(function() {
 var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
 lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
 var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
})();
</script>

</body>
</html>