<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', 'IndexController@index');
Route::get('privacy-policy', 'IndexController@privacyPolicy');
Route::get('terms-and-conditions', 'IndexController@termsAndConditions');

// THANK YOU
Route::get('uk-standard-thank-you', 'IndexController@ukstandardthankyou');
Route::get('eu-standard-thank-you', 'IndexController@eustandardthankyou');
Route::get('uk-full-clearance-thank-you', 'IndexController@ukfullclearancethankyou');
Route::get('uk-full-clearance-and-analysis-thank-you', 'IndexController@ukfullclearanceanalysisthankyou');

// Packages
Route::get('package/uk-standard', 'IndexController@ukStandard');
Route::get('package/eu-standard', 'IndexController@euStandard');
Route::get('package/uk-full-clearance', 'IndexController@ukFullClearance');
Route::get('package/uk-full-clearance-and-analysis', 'IndexController@ukClearance');


// Helpful
Route::get('trade-mark-applications-and-registration', 'IndexController@trademarkApplication');
Route::get('changes-and-renewals-of-your-registered-trade-marks', 'IndexController@changesAndRenewals');
Route::get('the-trademark-companys-service-and-how-we-work', 'IndexController@theTrademark');
Route::get('what-is-a-trademark', 'IndexController@whatIsATrademark');
Route::get('classification-of-goods', 'IndexController@classificationOfGoods');


//contact us
Route::group(['prefix' => 'send'], function () {
    Route::post('contactus', function () {

        $data['fields'] = Input::get();
        Mail::send('emails.contactus', $data, function($message)
        {
            $message->subject('Contact Us Form');
            $message->from(Input::get('email'), Input::get('name'));
            $message->to('marvz73@gmail.com');

        });

        return redirect('/')->with('message', 'Successfully send!');

    });

});














// Route::get('/', function () {
//     return view('welcome');
// });
