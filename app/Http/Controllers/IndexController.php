<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
         return view('partials.home');
    }



    public function privacyPolicy()
    {
        return view('partials.privacy-policy');
    }

    public function termsAndConditions()
    {
        return view('partials.terms-and-conditions');
    }


    // Packages
    public function ukStandard()
    {
        return view('partials.packages.uk-standard');
    }
    public function euStandard()
    {
        return view('partials.packages.eu-standard');
    }
    public function ukFullClearance()
    {
        return view('partials.packages.uk-full-clearance');
    }
    public function ukClearance()
    {
        return view('partials.packages.uk-full-clearance-and-analysis');
    }



    // THANK YOU
    public function ukstandardthankyou()
    {
        return view('partials.thankyou.ukstandard-thankyou');
    }
    public function eustandardthankyou()
    {
        return view('partials.thankyou.eustandard-thankyou');
    }
    public function ukfullclearancethankyou()
    {
        return view('partials.thankyou.ukfullclearance-thankyou');
    }
    public function ukfullclearanceanalysisthankyou()
    {
        return view('partials.thankyou.ukfullclearanceandanalysis-thankyou');
    }



    // HELPER
    public function trademarkApplication()
    {
        return view('partials.helpful.trademark-applications-and-registrations');
    }

    public function changesAndRenewals()
    {
        return view('partials.helpful.changes-and-renewals');
    }
    public function theTrademark()
    {
        return view('partials.helpful.the-trademark-company');
    }
    public function whatIsATrademark()
    {
        return view('partials.helpful.what-is-trademark');
    }
    public function classificationOfGoods()
    {
        return view('partials.helpful.classification-of-goods');
    }






    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
