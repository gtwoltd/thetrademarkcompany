<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'thetrade_blog');

/** MySQL database username */
define('DB_USER', 'thetrade_u');

/** MySQL database password */
define('DB_PASSWORD', 'fixit2014');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'g)]+Te0R,>CwT/!`}Am|wM&|5i|L15gj7uX (Jr:5{?7Y|*`HyxZ+{<k@X.)>KzT');
define('SECURE_AUTH_KEY',  '6iF_HS[T;5)TwF&S2#?~(/}Xo(?Q hYciiHcsiBD(iw7b+)N7Ta?-zf?=:cO9?wW');
define('LOGGED_IN_KEY',    '`SefDwv|-(*F?9tJ8hc4,4kUEa4l:hTTMp[Z~>a|X/t7c!,odn+%}ZyEx6ehY|X9');
define('NONCE_KEY',        ',`s+J|NNUPtQg|>*WAOanThf[GolTpK~pr)yjjU6iz-p@k}ojJ%AMbkosK04xt5=');
define('AUTH_SALT',        'W_8my-L>[Sdt)J7F<Kyi5Tms+}%t<}J`403iU<)-o^T+1_K>^O`TP]KFk-andVmA');
define('SECURE_AUTH_SALT', 'l7-,ZBqsuHFv[,$Ep3NQ.-K=_| Pr0(W8fZABGIW 6TQl07?wBS?.5k*x`,hA`#M');
define('LOGGED_IN_SALT',   'A&:C!)^FcqK3!a)mQ|>`u>_|-;8D]F^ekiggO-A4zis%X-N%=EL@FM -zzl&n)I#');
define('NONCE_SALT',       '^`JBa!i2:-#-(#R{*+Q(IYb4t+NL_|@)+8S99y[-B=v3Kvm7LEEV;M4+.9lORbb=');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
