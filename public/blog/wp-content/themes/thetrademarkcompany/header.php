<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package thetrademarkcompany
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-60073042-11', 'auto');
  ga('send', 'pageview');
</script>
</head>

<body <?php body_class(); ?>>

<nav class="navbar navbar-default cp-bg-main navbar-fixed-top topnav" role="navigation">
        <div class="container topnav">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header cp-padding-nav cp-font-lg ">
                <a class="navbar-brand topnav cp-strong hidden-xs" href="http://thetrademarkcompany.co.uk"><img src="http://thetrademarkcompany.co.uk/assets/img/logo-sm.png" alt="trademark company logo" title="The Trademark Company Logo"></a>
                <a href="index.html" class="cp-white visible-xs" style="padding-left:20px;margin-top:12px;float:left">Trademark <span class="cp-green">Company</span></a>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse cp-padding-nav" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-left cp-font-md">
                    <li>
                        <a href="<?=getsiteurl('#about')?>" class="cp-green">About Us</a>
                    </li>
                    <li>
                        <a href="<?=getsiteurl('#step')?>" class="cp-green">3 Step Process</a>
                    </li>
                    <li>
                        <a href="<?=getsiteurl('#classes')?>" class="cp-green">Classes</a>
                    </li>
                    <li>
                        <a href="<?=getsiteurl('#pricing')?>" class="cp-green">Pricing</a>
                    </li>
                    <li class="dropdown">
                         <a href="#" class="dropdown-toggle cp-green" data-toggle="dropdown" aria-expanded="false">Packages <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li>
                              <a href="<?=getsiteurl('package/uk-standard')?>">UK Standard</a>
                            </li>       
                            <li>
                              <a href="<?=getsiteurl('package/eu-standard')?>">EU Standard</a>
                            </li>   
                            <li>
                              <a href="<?=getsiteurl('package/uk-full-clearance')?>">UK Full Clearance</a>
                            </li>   
                            <li>
                              <a href="<?=getsiteurl('package/uk-full-clearance-and-analysis')?>">UK Full Clearance and Analysis</a>
                            </li>   
                        </ul>
                    </li>

                    <li>
                        <a href="<?=getsiteurl('#testimonials')?>" class="cp-green">Testimonials</a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle cp-green" data-toggle="dropdown" aria-expanded="false">Helpful Information <span class="caret"></span></a>
                         <ul class="dropdown-menu">
                            <li>
                              <a href="<?=getsiteurl('trade-mark-applications-and-registration')?>">Trademark applications and registration</a>
                            </li>  
                            <li>
                              <a href="<?=getsiteurl('changes-and-renewals-of-your-registered-trade-marks')?>">Changes and renewals of your registered trade marks</a>
                            </li>  
                            <li>
                              <a href="<?=getsiteurl('the-trademark-companys-service-and-how-we-work')?>">The Trademark Company’s service and how we work</a>
                            </li>  
                            <li>
                              <a href="<?=getsiteurl('/what-is-a-trademark')?>">About Trademarks</a>
                            </li>  
                            <li>
                              <a href="<?=getsiteurl('/classification-of-goods')?>">Classification of goods</a>
                            </li>  
                        </ul>
                    </li>

                     <li>
                        <a href="<?=getsiteurl('blog')?>" class="cp-green">Blog</a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="cp-white cp-font-md">0345 052 2759</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

<div class="container">
