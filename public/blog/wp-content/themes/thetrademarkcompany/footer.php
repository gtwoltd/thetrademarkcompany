<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package thetrademarkcompany
 */

?>

</div><!-- .container -->

 <!-- Footer -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="col-lg-3 col-md-6 col-sm-6 visible-xs">
                        <ul style="list-style-type: none;margin-left:-39px;float:left">
                            <li>
                                <a class="cp-grey cp-deco-none">Find us on</a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-facebook-square fa-2x cp-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter-square fa-2x cp-twitter"></i></a>
                            </li>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 visible-xs" style="margin-left:170px">
                        <ul class="list-inline">
                            <li>
                                <a class="cp-grey cp-deco-none">Call us to find out more</a>
                            </li>
                        </ul> 
                        <div style="margin-top:-15px">
                            <h3>
                                <span class="cp-white">0000 0000 000</span>
                            </h3>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <ul class="list-inline">
                            <li>
                                <a class="cp-green" href="<?=getsiteurl('/#about')?>">About</a>
                            </li>
                            <li>
                                <a class="cp-green" href="<?=getsiteurl('/#step')?>">3 Step Process</a>
                            </li>
                            <li>
                                <a class="cp-green" href="<?=getsiteurl('/#classes')?>">Classes</a>
                            </li>
                            <li>
                                <a class="cp-green" href="<?=getsiteurl('/#pricing')?>">Pricing</a>
                            </li>
                            <li>
                                <a class="cp-green hidden-xs" href="<?=getsiteurl('/#testimonials')?>">Testimonials</a>
                            </li>
                        </ul>
                        <!-- <p class="cp-grey small">Rates are variable dependant on circumstances and will be discussed in full once an assessment has been made.</p> -->
                        <ul class="list-inline">
                            <li>
                                <a class="cp-green" href="<?=getsiteurl('terms-and-conditions')?>">Terms and Conditions</a>
                            </li>
                            <li>
                                <a class="cp-green" href="<?=getsiteurl('privacy-policy')?>">Privacy Policy</a>
                            </li>
                        </ul>
                        <p class="copyright cp-grey text-muted small"> &copy; 2015 <a class="cp-deco-none">The Trademark Company</a>. All Rights Reserved.</p>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 hidden-xs">
                        <ul style="list-style-type: none;">
                            <li>
                                <a class="cp-grey cp-deco-none">Find us on</a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-facebook-square fa-2x cp-facebook"></i></a>
				<a href="https://www.linkedin.com/company/10047754?trk=tyah&amp;trkInfo=clickedVertical%3Acompany%2Cidx%3A1-1-1%2CtarId%3A1436449333651%2Ctas%3Athe%20trademark%20company%20uk"><i class="fa fa-linkedin-square fa-2x cp-linkedin"></i></a>
                                <a href="#"><i class="fa fa-twitter-square fa-2x cp-twitter"></i></a>
                            </li>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 hidden-xs">
                        <ul class="list-inline">
                            <li>
                                <a class="cp-grey cp-deco-none">Call us to find out more</a>
                            </li>
                        </ul> 
                        <div style="margin-top:-15px">
                            <h3>
                                <span class="cp-white">0345 052 2759</span>
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>


<?php wp_footer(); ?>

</body>
</html>
